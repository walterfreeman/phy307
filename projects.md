---
layout: page
navtitle: Projects
category: top
title: Projects
permalink: projects/
use_math: true
---

<a href="project7-1.html">Project 7, parts due before class November 7 and November 14; final submission due by December 15</a>

<a href="project6.html">Project 6, due before class October 31</a>

<a href="project5.html">Project 5, due before class October 24</a>

<a href="project4.html">Project 4, due before class October 15</a>; your code and data must be done by the end of class October 10

<a href="project3.html">Project 3, due before class October 3</a> 

<a href="project2.html">Project 2, due before class September 24</a>

<a href="project1.html">Project 1, due Wednesday, September 11 by the end of the day</a> 

<a href="project0.html">Project 0, due Monday, September 2 by the end of the day</a> 

