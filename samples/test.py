#diff eq rk2 error code

from numpy import *

#k = 0.1/60
k = 0.1
ambient = 30
start = 100

def DE(temp):
    DE = -k * (temp - ambient)
    return DE

for n in arange (0,3,1):
    stepsize = float(10**(-float(n)))
    t = 0
    tmax = 5
    temp = start
    for t in arange (0,tmax-stepsize/2,stepsize):
        slope_start = DE(temp)
#        tmid = temp + (DE(temp) * stepsize * 0.5)
        tmid = temp + slope_start * stepsize * 0.5
        slope_mid = DE(tmid)
#        newtemp = temp + (DE(tmid) * stepsize)
        newtemp = temp + slope_mid * stepsize
        print ("time = %e T_old = %e slope_start = %e T_mid = %e slope_mid = %e T_new = %e" % (t, temp, slope_start, tmid, slope_mid, newtemp))
#        print(DE(temp), DE(tmid))
        temp = newtemp
    analytic = ambient + ((start - ambient) * exp(-k * tmax))
    print ("End of loop: final time is %e, final temp is %e, analytic temp is %e" % (tmax, temp, analytic))
    error = abs(analytic - temp)
    print(stepsize,error)
