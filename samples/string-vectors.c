#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "vector.h"

double t;

// #define ONE_PERIOD_ONLY // uncomment this to do only one period, then print stats and exit. 
void get_forces(vector pos[], vector F[], int N, double k, double r0) // pointers here are array parameters
{
  int i;
  double r;
  vector sep;
  F[0]=F[N]=vector(0,0,0);
  double U=0,E=0,T=0;
 
  for (i=1;i<N;i++)
  {
    // left force
    sep = pos[i-1] - pos[i];
    r=norm(sep);
    F[i] = (r - r0) * k * sep/r;

    // right force
    sep = pos[i+1] - pos[i];
    r=norm(sep);
    F[i] += (r - r0) * k * sep/r;
  }
}

void evolve_leapfrog(vector pos[], vector vel[], int N, double k, double m, double r0, double dt) 
{
  int i;

  vector F[N+1];
  
  for (i=1;i<N;i++)
	  pos[i] += vel[i] * dt/2;
  
  
  get_forces(pos,F,N,k,r0);

  for (i=1;i<N;i++)
    vel[i] += F[i]/m*dt;

  for (i=1;i<N;i++)
	  pos[i] += vel[i] * dt/2;
}


// Students might not be familiar with pass-by-reference as a trick for returning multiple values yet. 
// Ideally they should be coding this anyway, and there are a number of workarounds, in particular 
// just not using a function for this.
void get_energy(vector pos[], vector vel[], int N, double k, double m, double r0, double *E, double *T, double *U)
{
  *E=*T=*U=0;
  int i;
  double r;
  for (i=0;i<N;i++)
  {
    *T+=0.5*m*vel[i]*vel[i];
    r = mag(pos[i]-pos[i+1]);
    *U+=0.5*k*(r-r0)*(r-r0);
  }
  *E=*T+*U;
}

// function to encapsulate determining whether we need to shovel another frame to the animator. delay is the delay in msec.
// I usually aim to hide library calls, like clock() and CLOCKS_PER_SEC, from students in the beginning, since it's not really
// relevant to their development of computational skills, which are what I really care about.
int istime(int delay)
{
  static int nextdraw=0;
  if (clock() > nextdraw)
  {
    nextdraw = clock() + delay * CLOCKS_PER_SEC/1000.0;
    return 1;
  }
  return 0;
}

int main(int argc, char **argv)
{
	printf("font large\n");
  int i,N=80; //  number of links, not number of nodes!! Careful for the lurking fencepost errors
  int modenumber=3; // put in some defaults just in case 
  double dt=2e-6, amplitude=0.1;
  double stiffness=10, density=1, length=1; // unstretched properties of original string
  double k, m, r0; // properties of single string
  double tension=1,Ls;
  int frame=0, frameskip;
  double modeamp=0, modeamplast=0, modeamplastlast=0;

  if (argc < 10) // if they've not given me the parameters I need, don't just segfault -- tell the user what to do, then let them try again
  {
    printf("!Usage: <this> <N> <modenumber> <dt> <amplitude> <stiffness> <density> <length> <tension> <frameskip>\n"); 
    exit(0);
  }
  N=atoi(argv[1]);
  modenumber=atoi(argv[2]);
  dt=atof(argv[3]);
  amplitude=atof(argv[4]);
  stiffness=atof(argv[5]);
  density=atof(argv[6]);
  length=atof(argv[7]);
  tension=atof(argv[8]);
  frameskip=atof(argv[9]);

  double E, T, U;
  vector pos[N+1], vel[N+1];

  // compute microscopic properties from macroscopic ones 

  r0=length/N;
  m=density*length/N;
  k=stiffness*N/length;

  // figure out stretched length

  Ls=length + tension * length / stiffness;

  // make predictions based on what our freshman mechanics class taught us

  double density_stretched = density * length / Ls;
  double wavespeed = sqrt(tension/density_stretched);
  double period_predict = 2 * Ls / wavespeed / modenumber;
  double vym_last=0;

  int monitor_node = N/modenumber/2; // this is the node that we'll be watching to see when a period has elapsed.
  int nperiods=0;  

  for (i=0;i<=N;i++) // remember, we have N+1 of these
  {
	  pos[i]=vector(Ls*i/N, amplitude*sin(modenumber * M_PI * (double)i/N), 0);
	  vel[i]=vector(0,0,0);
  }

  // now, loop over time forever...
  printf("!N\tmode\tstiffness\tL\t\tamp.\t\tdens.\tTension\tT\tT_pred\tDelta\t\tFreq (Hz)\tFreq_pred\n"); // print header
  for (t=0;1;t+=dt)
  {
    modeamplastlast=modeamplast;
    modeamplast=modeamp;
    modeamp=0;
    for (i=1; i<N; i++)
    {
      modeamp += pos[i].y * sin(modenumber * M_PI * pos[i].x / Ls);
    }
    vym_last=vel[monitor_node].y;
    evolve_leapfrog(pos, vel,N,k,m,r0,dt);
    
    // "if we were going up, but now we're going down, then a period is complete"
    // this is crude and will fail if it has enough "wobble", but it's sufficient for this project. 
    if (modeamp-modeamplast < 0 && modeamplast-modeamplastlast > 0 && frame>5)
    {
      nperiods++;
      printf("!%d\t%d\t%.2le\t%.2le\t%.4le\t%.1le\t%.1le\t%.4lf\t%.4lf\t%.4le\t%.4le\t%.4le\n",N,
modenumber,stiffness,length,amplitude,density,tension,t,period_predict*nperiods,1-t/period_predict/nperiods,nperiods/t,1.0/period_predict);
    #ifdef ONE_PERIOD_ONLY
      printf("Q\n"); // kill anim before we die ourselves
      exit(1);       
    #endif
    }
    frame++; 
    if (frame % frameskip == 0) // wait 15ms between frames sent to anim; this gives us about 60fps.
//    if (istime(15))
    {
      printf("C 1 0 0\nc %lf %lf 0.02\nC 1 1 1\n",pos[monitor_node].x,pos[monitor_node].y); // draw a big red blob around the node we're watching
      for (i=0;i<=N;i++)
      {
        printf("C %le 0.5 %le\n",0.5+pos[i].y/amplitude,0.5-pos[i].y/amplitude); // use red/blue shading; this will make vibrations visible even if amp<<1
//	printf("ct3 %d %lf %lf 0 %lf\n",i,pos[i].x,pos[i].y,length/N/3); // draw circles, with radius scaled to separation
	printf("c3 %lf %lf 0 %lf\n",pos[i].x,pos[i].y,length/N/3); // draw circles, with radius scaled to separation
	if (i<N) printf("l %lf %lf %lf %lf\n",pos[i].x,pos[i].y,pos[i+1].x,pos[i+1].y); // the if call ensures we don't drive off the array
      }
      printf("T -0.5 -0.7\ntime = %lf\n",t); 
      get_energy(pos,vel,N,k,m,r0,&E,&T,&U);
      printf("T -0.5 -0.63\nenergy = %le + %le = %le\n",T,U,E);
      printf("F\n"); // flush frame
    }
  }
} 
