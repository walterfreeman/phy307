from numpy import *

def f(x):
    return sqrt(x)

def left_hand_integrate(lower_bound, upper_bound, width):
    total = 0
    for x in arange(lower_bound, upper_bound-width/2, width): # loop over left side of intervals
        y = f(x)
        total = total + y*width

    return total

def midpoint_integrate(lower_bound, upper_bound, width):
    total = 0
    for x in arange(lower_bound, upper_bound-width/2, width): # loop over midpoints
        y = f(x + width/2)
        total = total + y*width

    return total

def trapezoid_integrate(lower_bound, upper_bound, width):
    total = 0
    for x in arange(lower_bound, upper_bound-width/2, width): # loop over left side of intervals
        y1 = f(x) # left side
        y2 = f(x+width) # right side
        total = total + (y1+y2)/2 * width # area of trapezoid

    return total


# main program begins here
exact_result = 4/3 * sqrt(2) - 2/3

# iterate over stepsizes, starting at 1 and halving each time

h = 1
while (h > 1e-7):
#    numeric_result = (trapezoid_integrate(0, 2, h) + midpoint_integrate(0, 2, h)*2)/3
    numeric_result = midpoint_integrate(1, 2, h)
    error = abs(numeric_result - exact_result)
    print("Stepsize of",h," gives an error of", error)
    h=h/10
