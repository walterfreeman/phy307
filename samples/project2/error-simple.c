#include <stdio.h>
#include "integrators.h"
#include <math.h>

int main(void)
{
	int steps;
	double exact = cos(0) - cos(2);
	double h;
	double lhr_result, mr_result, trap_result, simpson_result;

	for (steps = 2; steps < 1e7; steps *= 2)
	{
		h = 2.0 / steps;
		lhr_result = lefthandrule_d(0, 2, steps);
	printf("lhr_d %e %e\n",h,fabs(lhr_result-exact));
	}
}
