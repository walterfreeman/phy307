./error-vs-stepsize > raw
#grep lhr_f raw | awk {'print $2,$3'} > lhr_error_float
grep lhr_d raw | awk {'print $2,$3'} > lhr_error_double
#grep mid_f raw | awk {'print $2,$3'} > mid_error_float
grep mid_d raw | awk {'print $2,$3'} > mid_error_double
#grep trap_f raw | awk {'print $2,$3'} > trap_error_float
grep trap_d raw | awk {'print $2,$3'} > trap_error_double
#grep simpson_f raw | awk {'print $2,$3'} > simpson_error_float
grep simpson_d raw | awk {'print $2,$3'} > simpson_error_double
