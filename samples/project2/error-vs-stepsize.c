#include <stdio.h>
#include "integrators.h"
#include <math.h>

int main(void)
{
	int steps;
	double exact = cos(0) - cos(2);
	double h;
	double lhr_result, mr_result, trap_result, simpson_result;

	for (steps = 2; steps < 5e8; steps *= 2)
	{
		h = 2.0 / steps;
		lhr_result = lefthandrule_d(0, 2, steps);
		mr_result = midpointrule_d(0, 2, steps);
		trap_result = trapezoidrule_d(0, 2, steps);
	        simpson_result = simpson_d(0, 2, steps);
		printf("lhr_d %e %e\n",h,fabs(lhr_result-exact));
		printf("mid_d %e %e\n",h,fabs(mr_result-exact));
		printf("trap_d %e %e\n",h,fabs(trap_result-exact));
		printf("simpson_d %e %e\n",h,fabs(simpson_result-exact));
//		lhr_result = lefthandrule(0, 2, steps);
////		mr_result = midpointrule(0, 2, steps);
//		trap_result = trapezoidrule(0, 2, steps);
//	        simpson_result = simpson(0, 2, steps);
//		printf("lhr_f %e %e\n",h,fabs(lhr_result-exact));
//		printf("mid_f %e %e\n",h,fabs(mr_result-exact));
//		printf("trap_f %e %e\n",h,fabs(trap_result-exact));
//		printf("simpson_f %e %e\n",h,fabs(simpson_result-exact));
	}
}
