double lefthandrule_d(double lowerbound, double upperbound, int steps);
double midpointrule_d(double lowerbound, double upperbound, int steps);
double trapezoidrule_d(double lowerbound, double upperbound, int steps);
double simpson_d(double lowerbound, double upperbound, int steps);

float lefthandrule(float lowerbound, float upperbound, int steps);
float midpointrule(float lowerbound, float upperbound, int steps);
float trapezoidrule(float lowerbound, float upperbound, int steps);
float simpson(float lowerbound, float upperbound, int steps);

