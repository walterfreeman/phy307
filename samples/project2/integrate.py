from numpy import *

def f(x):
    return (sin(x))

def midpoint(a, b, steps):
    total = float32(0)
    dx = (b-a) / steps
    for x in arange (a, b, dx):
        total = total + f(x + dx/2)*dx # midpoint

    return total


exact = cos(0) - cos(2)
steps = 1
while steps < 1e8:
    numeric = midpoint(0, 2, steps)
    print (2/steps, abs(numeric-exact))
    steps = steps * 2


