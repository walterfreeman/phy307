#include <math.h>

double lefthandrule_d(double lowerbound, double upperbound, int steps)
{
	double h = (upperbound - lowerbound) / steps;
	double sum = 0;

	for (double x=lowerbound; x < upperbound - h/2; x += h)
	{
		sum += sin(x) * h;
	}
	return sum;
}

double midpointrule_d(double lowerbound, double upperbound, int steps)
{
	double h = (upperbound - lowerbound) / steps;
	double sum = 0;

	for (double x=lowerbound; x < upperbound - h/2; x += h)
	{
		sum += sin(x+h/2) * h;
	}
	return sum;
}

double trapezoidrule_d(double lowerbound, double upperbound, int steps)
{
	double h = (upperbound - lowerbound) / steps;
	double sum = 0;

	for (double x=lowerbound; x < upperbound - h/2; x += h)
	{
		sum += (sin(x) + sin(h+x)) * h/2;
	}
	return sum;
}

double simpson_d(double lowerbound, double upperbound, int steps)
{
	return (trapezoidrule_d(lowerbound, upperbound, steps) + 2 * midpointrule_d(lowerbound, upperbound, steps)) / 3;
}

float lefthandrule(float lowerbound, float upperbound, int steps)
{
	float h = (upperbound - lowerbound) / steps;
	float sum = 0;

	for (float x=lowerbound; x < upperbound - h/2; x += h)
	{
		sum += sin(x) * h;
	}
	return sum;
}

float midpointrule(float lowerbound, float upperbound, int steps)
{
	float h = (upperbound - lowerbound) / steps;
	float sum = 0;

	for (float x=lowerbound; x < upperbound - h/2; x += h)
	{
		sum += sin(x+h/2) * h;
	}
	return sum;
}

float trapezoidrule(float lowerbound, float upperbound, int steps)
{
	float h = (upperbound - lowerbound) / steps;
	float sum = 0;

	for (float x=lowerbound; x < upperbound - h/2; x += h)
	{
		sum += (sin(x) + sin(h+x)) * h/2;
	}
	return sum;
}

float simpson(float lowerbound, float upperbound, int steps)
{
	return (trapezoidrule(lowerbound, upperbound, steps) + 2 * midpointrule(lowerbound, upperbound, steps)) / 3;
}
