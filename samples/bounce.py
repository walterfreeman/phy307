from numpy import *

position = array([0, 1])
velocity = array([5, 0])
g = array([0, -9.8])

dt = 0.01
while True:
    position = position + velocity * dt/2
    
    acceleration = g - velocity * 0.1

    velocity = velocity + acceleration * dt
    if (position[1] < 0):
        velocity[1] = velocity[1] * -1
        position[1] = position[1] * -1

    position = position + velocity * dt/2

    print("ct3 0",position[0], position[1], 0, 0.1)
    print("F")
