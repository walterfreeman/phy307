from numpy import *
g = 9.8
dt = 0.00001 # initially 0.01
L = 0.248  #Length of pendelum when period is equal to 1 second
theta = 0.349

Tanalytical = 2*pi*sqrt(L/g)
print ("Analytically predicted period is",Tanalytical)

omega = 0
alpha = 0
time = 0  #1,728 seconds have elapsed when changing the max angle of theta from 5 degrees to 20 degrees.
list = [ 0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 2]
def function(angle):
    alpha = (-g/L)*sin(angle)
    return alpha
for i in range(8):
    time = 0
    omega = 0
    alpha = 0
    theta = list[i]
    print ("Starting a simulation with starting angle ",theta)
    while omega <= 0:
        #Update position/angle
        theta = theta + omega * (dt/2)
        #Acceleration
        alpha = function(theta)
        #Update angular velocity
        omega = omega + alpha * dt
        #Update position/angle
        theta = theta + omega * (dt/2)
        x=-L*sin(theta)
        y=-L*cos(theta)
        time += dt
#        print("l",0.0, 0.0, x, y)  # draw a line)
#        print("c", x*1.05, y*1.05, 0.05) # draw a circle
#        print("t",x*1.5, y*1.5)
#        print("theta = %.2f" % (theta))   # these last two lines together draw some text
#        print("F") # flush frame
        #print("!",time,omega)
        #print(theta,dt)
        #print(omega,dt)
    print ("Halfperiod is",time)
    bigdelta = (time * 2) - 1
    print(abs(theta),abs(bigdelta)) #Small angle approximation works for the first 6 bigdelta's, but starts to change at 7 and 8 because the small angle approximation stops working
print("Q")
