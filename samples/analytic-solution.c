#include <stdio.h>
#include <math.h>

int main(void)
{
	double k=0.1; // per minute
	double T0=100, Ta=30, T;

	for (double t=0; t<10; t += 1.0/60.0)
	{
		T = Ta + (T0 - Ta) * exp(-k*t);
		printf("%e %e\n",t*60,T);

	}
}
