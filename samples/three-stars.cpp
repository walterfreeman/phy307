#include <math.h>
#include <stdio.h>
#include "vector.h"

vector accel(vector pos1, vector pos2, double m2)
{
    double G=4*M_PI*M_PI;
    vector sep=pos2-pos1;
    double r=mag(sep);

    return G*m2*sep/(r*r*r);
}

int main(void)
{
  double dt=0.00001;
  double m1=0.5;
  double m2=0.5;
  double m3=0.5;
  vector pos1(0.5, 0, 0);
  vector pos2(-0.5, 0, 0);
  vector pos3(0, 8, 0);
  vector vel1(0, 3, 0);
  vector vel2(0, -3, 0);
  vector vel3(0, 0, 2);

  vector center_velocity = (m1*vel1 + m2*vel2 + m3*vel3) / (m1+m2+m3);

  vel1 = vel1 - center_velocity;
  vel2 = vel2 - center_velocity;
  vel3 = vel3 - center_velocity;

  int steps=0;
  int stepsperframe = 2500;

  while (1)
  {
      // first position half-update
      pos1 = pos1 + vel1 * dt/2;
      pos2 = pos2 + vel2 * dt/2;
      pos3 = pos3 + vel3 * dt/2;

      // velocity update -- do each force separately
      vel1 = vel1 + (accel(pos1, pos2, m2) + accel(pos1, pos3, m3)) * dt;
      vel2 = vel2 + (accel(pos2, pos1, m1) + accel(pos2, pos3, m3)) * dt;
      vel3 = vel3 + (accel(pos3, pos1, m1) + accel(pos3, pos2, m2)) * dt;

      // first position half-update
      pos1 = pos1 + vel1 * dt/2;
      pos2 = pos2 + vel2 * dt/2;
      pos3 = pos3 + vel3 * dt/2;


      if (steps % stepsperframe == 0)
      {
	      printf("C 0.5 0.5 1\n"); // blueish
	      printf("ct3 0 %e %e %e 0.1\n",pos1.x,pos1.y,pos1.z);
	      printf("C 1 0.5 0.5\n"); // reddish
	      printf("ct3 1 %e %e %e 0.1\n",pos2.x,pos2.y,pos2.z);
	      printf("C 0.5 1 0.5\n"); // greenish
	      printf("ct3 2 %e %e %e 0.1\n",pos3.x,pos3.y,pos3.z);
	      printf("F\n");
      }

      steps=steps+1;
  }
}



