from numpy import *

dt = 0.01
theta = 3.1
omega = 0
g = 9.8
L = 1
t = 0

while t < 100:
    t = t + dt
    theta = theta + omega * dt/2
    omega = omega - g/L * sin(theta) * dt
    theta = theta + omega * dt/2
    print("l",0,0,sin(theta),-cos(theta))
    print("c",sin(theta),-cos(theta),0.1)
    print("F")


