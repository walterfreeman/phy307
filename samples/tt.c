//Left Hand Rule Method using function called trapezoidsum
//
//Bin Width Looped from 1 to 10^(-7)
//2 Bins give Width of 1
//20000000 Bins give Width of 10^(-7)  
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
float trapezoidsum(int N, float x_init, float x_fin);//prototype
int main() {
    float actual_int = 2.0*(sin(1)*sin(1));
    float error = 0;
    float x_init = 0;
    float x_fin = 2;
    for (int N = 2; N <= 20000000; N=10*N)
    {
        float Riemann_sum;
        Riemann_sum = trapezoidsum(N, x_init, x_fin);
        error = fabs(actual_int - Riemann_sum);
        printf("%d %lf\n", N, error);
    }
    return 0;
}
float trapezoidsum(int N, float x_init, float x_fin)
{
    float delta_x = (x_fin - x_init)/N;
    float sum = 0.0;
    for (int i = 0; i < N; i++) {
        float x_1 = x_init + i*delta_x;
        float x_2 = x_init + (i+1)*delta_x;
        sum = sum + 0.5*(sin(x_1)+sin(x_2))*delta_x;
    }
    return sum;
}
