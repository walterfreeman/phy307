// First thing that's needed is euler, as RK2 calls euler.
#include <stdio.h>
#include <stdlib.h>
double euler(double k, double t) {  // k = magical constant
                                    // t = object temperature - room temperature
    double y = -1 * k * (t);
    return y; // returns slope of the curve
}                       
struct tuple
{
    double* fst;
    double snd; 
    double thd;
};
// Should have used a timeFinder function and then use that for inputted times.
struct tuple eulerPlotter(double k, double to, double tr, int inc, double epsilon) { // k is over seconds
    struct tuple drop; // fst = temp to plot, snd = temp drop
    int seconds = 0;
    int emergencyBreak = 1000000;
    struct tuple output; // For RK2, bit of a hack, but alas.
    int const allocatedMem = 1000;
    double *temperatureOverTime = (double *)malloc(allocatedMem * sizeof(double)); // There are problems if minutes goes too long 
    int index = 0;
    while (to > epsilon) // using a while loop as the result will always return to room temperature (after infinity)
    {
        double tempDrop = inc * euler(k, to-tr);
        seconds += inc; // stepsize
        temperatureOverTime[index] = to; 
        index ++;
        to += tempDrop;
        emergencyBreak -= 1;
        if (emergencyBreak <= 0) {
            printf("Emergency Break");
            break; // Avoiding accidental infinite loops.
        }
        if (seconds >= allocatedMem) {
            printf("Allocate more memory to temperatureOverTime"); // C arrays suck.
            break;
        }
    }
    output.fst = temperatureOverTime;
    output.snd = seconds;
    output.thd = index;
    return output; // If run in Euler mode, this return value is thrust into the void.
    }
    
int main(int argc, char const *argv[])
{
    double k = 1.0/600; // in seconds
    double to = 100;
    double tr = 30;
    int inc = 1;
    double epsilon = 0.001;
    struct tuple output = eulerPlotter(k, to, tr, inc, epsilon);
    double intervals = output.snd / output.thd;
    double currInterval = 0;
    printf("\nthis is length of list: %f\n", output.thd);
    for (int i = 0; i < output.thd; i++)
    {   
        printf("%f --- %f\n", currInterval, output.fst[i]); // functional programming and lists
        currInterval += intervals;
    }
    return 0;
}
