#include <stdio.h>
#include <math.h>

int main(void)
{
	double T=100, Ta=30, k=0.1, dt=0.3, t=0;

	while (t < 30)
	{
		printf("%e %e\n",t,T);
		T = T - (T-Ta)*k*dt;
		t = t + dt;
	}
}
