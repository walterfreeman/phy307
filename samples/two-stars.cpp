#include <math.h>
#include <stdio.h>
#include "vector.h"

vector accel(vector pos1, vector pos2, double m2)
{
    double G=4*M_PI*M_PI;
    vector sep=pos2-pos1;
    double r=mag(sep);

    return G*m2*sep/(r*r*r);
}

int main(void)
{
  double dt=0.00001;
  double m1=1;
  double m2=2.2;
  vector pos1(2, 0, 0);
  vector pos2(0, 0, 0);
  
  vector vel1(0, 6, 0);
  vector vel2(0, 0, 1);

  vector center_velocity = (m1*vel1 + m2*vel2) / (m1+m2);

  vel1 = vel1 - center_velocity;
  vel2 = vel2 - center_velocity;
  
  int steps=0;
  int stepsperframe = 100;

  while (1)
  {
      // first position half-update
      pos1 = pos1 + vel1 * dt/2;
      pos2 = pos2 + vel2 * dt/2;

      // velocity update -- do each force separately
      vel1 = vel1 + accel(pos1, pos2, m2) * dt;
      vel2 = vel2 + accel(pos2, pos1, m1) * dt;

      // first position half-update
      pos1 = pos1 + vel1 * dt/2;
      pos2 = pos2 + vel2 * dt/2;


      if (steps % stepsperframe == 0)
      {
      printf("C 0.5 0.5 1\n"); // blueish
      printf("ct3 0 %e %e %e 0.1\n",pos1.x,pos1.y,pos1.z);
      printf("C 1 0.5 0.5\n"); // reddish
      printf("ct3 1 %e %e %e 0.1\n",pos2.x,pos2.y,pos2.z);
      printf("F\n");
      }

      steps=steps+1;
  }
}



