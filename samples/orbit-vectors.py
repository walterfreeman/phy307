from numpy import *

G = 4*pi*pi
M = 1

position = array([1, 0])
velocity = array([0, 5])

dt = 0.005

while True:
    position = position + velocity * dt/2

    r = linalg.norm(position)
    rhat = position / r
    acceleration = G*M/(r*r) * (-rhat)
    velocity = velocity + acceleration * dt

    position = position + velocity * dt/2

    print ("c",position[0], position[1], 0.05)
    print ("c",0,0,0.1)
    print ("F")


