from numpy import *

G = 4*pi*pi

position = array([1, 0, 0])
velocity = array([0, 5, 1])

dt = 0.003

while True:
    position = position + velocity * dt/2

    r = linalg.norm(position)
    velocity = velocity - G*position/(r*r*r) * dt

    position = position + velocity * dt/2

    print ("c3",0,0,0,0.1)
    print ("ct3 0",position[0],position[1],position[2],0.05)
    print ("F")
