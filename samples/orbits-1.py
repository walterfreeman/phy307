from numpy import *

G = 4*pi*pi
M = 1

position = array([1.0, 0.0, 0.0])  
velocity = array([0.0, 5.0, 2.0])
accel = array([0.0, 0.0, 0.0])

dt = 0.002

while True:
    position += velocity * dt/2

    radius = linalg.norm(position) # or sqrt(dot(position, position)) 
    accel = G*M / (radius**2) * (-position / radius)
    velocity += accel * dt

    position += velocity * dt/2

    print("c3 0 0 0 0.08")
    print("ct3",0,position[0],position[1],position[2],0.05)
    print("F")
