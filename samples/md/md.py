from numpypy import *

def vel_init_2D(vel, amount):
    for i in range(N):
        vel[i, 0]=random.random()*amount-amount/2
        vel[i, 1]=random.random()*amount-amount/2


def grid_init_2D(pos, N, spacing):
    dimension = int((N**0.5))

    for i in range(N):
        ii = i // dimension
        jj = i % dimension
        x = -(spacing*dimension)/2 + ii*spacing
        y = -(spacing*dimension)/2 + jj*spacing
        pos[i, 0] = x
        pos[i, 1] = y


def draw_box_2D():
    halfL=L/2
    print("l3",-halfL,-halfL,0,halfL,-halfL,0)
    print("l3", halfL,-halfL,0,halfL, halfL,0)
    print("l3", halfL, halfL,0,-halfL, halfL,0)
    print("l3",-halfL, halfL,0,-halfL, -halfL,0)

def animate(pos, N):
    draw_box_2D()
    for i in range(N):
        print("c3",pos[i,0],pos[i,1],0,0.01)
    print ("F")

repelpower = 12
attractpower = 6

def force(r):   # negative attracts, positive repels
    F = (r0/r)**repelpower
    F += -(r0/r)**attractpower
    return F

def potential(r):
    U = r0**repelpower / (r**(repelpower-1)) / (repelpower-1)
    U += -r0**attractpower / (r**(attractpower-1)) / (attractpower-1)
    return U

def potential_energy(pos):
    U = 0
    for i in range(N):
        for j in range(i):
            U += potential(linalg.norm(pos[i]-pos[j]))
    return U

def kinetic_energy(vel):
    T = 0
    for i in range(N):
        T += 0.5 * m * (vel[i,0]**2 + vel[i,1]**2)
    return T


def vel_update(pos, vel, dt):
    for i in range(N):
        for j in range(i):
            sep = pos[i] - pos[j]
            r = linalg.norm(sep)
            rhat = sep / r
            F = force(r) * rhat
            vel[i] += F/m * dt
            vel[j] -= F/m * dt

def check_walls(pos, vel):
    impulse_here = 0
    halfL=L/2
    for i in range(N):
        for j in range(2):
            if (pos[i,j] < -halfL and vel[i,j] < 0):
                vel[i,j] *= -1
                impulse_here += abs(2*vel[i,j]*m)
            if (pos[i,j] > halfL and vel[i,j] > 0):
                vel[i,j] *= -1
                impulse_here += abs(2*vel[i,j]*m)
    return impulse_here


L=2
N=120
m=1
r0=0.05
dt=1e-2
pos = zeros((N, 2))
vel = zeros((N, 2))
grid_init_2D(pos, N, r0)
vel_init_2D(vel, 0.6)
impulse = 0
T_last = 0
U_last = 0
step = 0
time = 0
T_accum = 0

while (True):

    pos = pos + vel * dt/2
    vel_update(pos, vel, dt)
    pos = pos + vel * dt/2
    time += dt

    impulse += check_walls(pos, vel)
    pressure = impulse / time / (4*L)
    T_last = kinetic_energy(vel) # do this outside the loop; it's fast

    T_accum += T_last

    step+=1

    if (step % 1 == 0):
        U_last = potential_energy(pos)
        animate(pos, N)
        print ("T -0.9 -0.9")
        print ("Energy : %.6e + %.6e = %.6e" % (T_last,U_last,T_last+U_last))
        print ("T -0.9 -0.95")
        print ("PV : %.5e   NkT : %.5e" % (pressure * L*L, T_accum/step))



