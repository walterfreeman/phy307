#include <stdio.h>
#include <math.h>

int main(void)
{
	double T=100, Ta=30, k=0.1, dt=3, t=0, Thalf;

	while (t < 30)
	{
		printf("%e %e\n",t,T);
		Thalf = T - (T-Ta)*k*dt/2;
		T = T - (Thalf-Ta)*k*dt;
		t = t + dt;
	}
}
