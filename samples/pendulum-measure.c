#include <stdio.h>
#include <math.h>

double g = 9.8;
double L = 1;

void euler_cromer_step(double &theta, double &omega, double dt)
{
	theta = theta + omega * dt;
	omega = omega - g/L * sin(theta) * dt;
}

void leapfrog_step(double &theta, double &omega, double dt)
{
	theta = theta + omega * dt/2;
	omega = omega - g/L * sin(theta)*dt;
	theta = theta + omega * dt/2;
}

double findperiod(double amplitude, double dt)
{
	double theta = amplitude, omega = 0, t = 0;

	int framedelay = 50.0/(60 * dt);   // what fraction of real time we should run at
	int framecounter = 0;

	while (theta >= 0)
	{
		leapfrog_step(theta, omega, dt);
		t = t + dt;
		if (framecounter % framedelay == 0)
		{
			printf ("l 0 0 %e %e\n", L*sin(theta), -L*cos(theta));
			printf ("c %e %e 0.05\n",L*sin(theta), -L*cos(theta));
			printf ("F\n");
		}
		framecounter++;
	}
        return 4 * (t - theta/omega);    // "last step interpolation"
}




int main(void)
{
	double dt=1e-3;
	double amplitude = 3;
        double period;

	double analytical_period = 2*M_PI*sqrt(L/g);

	while (amplitude > 1e-5)
	{
		period = findperiod(amplitude, dt);
		//printf("!%e %e\n",amplitude,fabs(period - analytical_period));
		printf("!%e %e\n",amplitude,(period - analytical_period));
		amplitude = amplitude * 0.8;
	}
}

