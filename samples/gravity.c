#include <stdio.h>
#include <math.h>
#include "vector.h"

int main(void)
{
	vector position(1, 0, 0);
	vector velocity(0, 5, 0);

	double dt=0.002;
	double G = 4*M_PI*M_PI;

	double r;

	while (1)
	{
		position = position + velocity * dt/2;
		
		r = magnitude(position);
		velocity = velocity - G*position/(r*r*r);

		position = position + velocity * dt/2;

		printf("c3 0 0 0 0.1\n");
		printf("ct3 0 %e %e %e 0.05\n",position.x, position.y, position.z);
		printf("F\n");
	}
}
