from numpy import *

theta = 0
framecounter = 0
frames_per_draw = 4
while True:
    theta = theta + 0.05
    x=cos(theta)
    y=sin(theta)
    framecounter = framecounter + 1
    if (framecounter % frames_per_draw == 0):
        print("l",0.0, 0.0, x, y)  # draw a line
        print("c", x ,y, 0.05)      # draw a circle
        print("F")                 # flush frame
