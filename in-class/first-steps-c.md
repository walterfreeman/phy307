---
layout: page
category: top
title: First steps 
permalink: first-steps.html
usemathjax: true
---

### First steps

You should do the following to get started with Physics 307:

1. Follow the <a href="notes/laptop.html">Setup Guide</a> to get everything set up on your laptop
2. Read the <a href="notes/language-comparison.html">language comparison</a> and decide whether you want to start out using C or Python. (Either one is fine, and you can switch later.)
3. Find a text editor that you like to use.
  * `nano`: very simple and accessible text-mode editor but without many features. Available on all operating systems out of the box.
  * `kate` or `gedit`: Friendly graphical text editors available on Linux. Recommended if you are using Linux (native or Windows).
  * <a href="https://www.sublimetext.com/download">Sublime Text Editor</a>: available for Linux or Mac. Installation will take about 5 minutes.
 
3. Read the introduction to your chosen programming language (<a href="notes/c.html">C</a> or <a href="notes/python.html">Python</a>). When you encounter
example programs, type them into your editor and run them. Then, to see that you understand what they do, change the code a bit and run them again. *Note: 
It is best if you type the code into an editor yourself rather than copy/pasting the code from the notes. The "muscle memory" aspect of programming is 
a real one, and you'll learn best and fastest if you do all the typing yourself.*


