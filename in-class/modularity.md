---
layout: page
title: Modularity
permalink: in-class/modularity.html
usemathjax: true
---

### Pair Programming Exercise: Modularity

This is an exercise that you'll do along with a partner. When you're done, you'll mostly 
have the code for the second part of Project 1.

It is designed to teach you how to use <a href="notes/function.html">functions</a> to 
create programs that are *modular*. This is a key programming practice that will help you
break a large task ("count all the prime numbers up to a million") into several smaller
tasks, and then put those smaller tasks together to solve a larger problem.

Consider a difficult engineering task: building a car. Very few engineers can hold all
the components involved in building a car in their heads at the same time -- or even
understand them all! But they can treat different components as "black boxes" that have
been built and tested. One engineer might design a body and test its aerodynamics in a
wind tunnel; another might design the motors and test them using electricity from the 
power grid; yet another may design and test a battery that will provide electricity on the road. 
Then they all get together and figure out how to combine the individual, tested parts to
solve the bigger problem. 

For this project, you should work together with a partner. Do the following steps:


#### Step 1
Connect with your partner on the SU Physics Slack channel or the PHY307 Discord. You'll need this to send code back and forth.


#### Step 2: checking if a number is divisible by another number

* Each person should write a function called `test_divisible()` that takes two arguments: a number and a divisor. It should return
1 if the number is evenly divisible by the divisor and should return 0 if
it is not. (If both you and your partner are familiar with "boolean" variables with values `True` and `False`, you can use those instead.)
Add a comment to your function specifying what it does and who wrote it (i.e. your name).

* Each person should send the code for your `test_divisible()` function to their partner. They are not allowed to change it!

* Test your partner's `is_divisible` function to see if it works. You aren't allowed
to change your partner's code; if it doesn't quite work, tell them what you saw in your
testing and work with them to get it fixed.

#### Step 3: checking if a number is prime

* Using the `test_divisible()` function, now write a function called `test_prime()` that takes one argument, and returns 1 if the number is prime but 0 if it is not prime.

* Once you're done, exchange code with your partner once you're both done. Compare the approaches you've each taken.

* Test your partner's code. Make sure that it does the right thing -- correctly assessing whether numbers are prime or not.

#### Step 4: counting prime numbers

* Now that you have your partner's `test_prime()` function (which uses your own `test_divisible()` code inside!), write one more function called `count_primes()`.
This function should take one argument, then return the number of prime numbers less than that number. 

* Once you've done that, each of you should run your code to count the prime numbers up to some large value (100,000 or more) and compare the result to see if you agree.

* Once you've done that, discuss with each other how you might get your program to perform better / go faster. A few hints: 1) you can use `break();` (in C) or `break` in Python
to exit a loop prematurely, and 2) if you want to round a number down to the next lowest integer, you can use `int(x)` in Python or `(int)x` in C. 

* If you're stuck, using only a simple calculator (not Python or C), you and your partner should determine if 323 is prime. Then determine if 899 is prime.

#### Step 5: checking in

Send a Slack direct message to me (Walter Freeman) on the SU Physics slack, or a Discord direct message, including the following:

* Who you worked with
* A brief description of your experience, including anything that was difficult or any roadblocks you overcame
* How many prime numbers you found counting up to what limit
* How long that took, and what optimizations you made
