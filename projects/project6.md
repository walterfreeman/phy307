---
layout: page
title: Project 6
permalink: projects/project6.html
use_math: true
---
<center>

This assignment is posted as a <a href="hw-gravity2.pdf">PDF</a> so the mathematics looks better. It is due before class Thursday, October 31.

</center>
