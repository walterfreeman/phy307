\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{amsmath}
\usepackage{fullpage}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\begin{document}
\Large
\centerline{\sc{Physics 307 Project 7, Part 1}}
\centerline{Hooke's Law Forces (due before class 7 November)}
\normalsize

The capstone project for this course is to create a computer model of a guitar string, then use it to study phenomena that
real strings exhibit but that aren't typically studied in PHY360 or any other physics class.

We're going to model the guitar string as a series of point masses  that are connected by springs that follow Hooke's law, holding the two masses on the ends fixed. By increasing the number of these masses, we can simulate a flexible, elastic object like a guitar string and then study its properties.

We're going to build up to this in several parts. While we could start with the theory, it's probably more fun to {\it build the thing} first, then figure out how to match its properties to a real string with a known elastic modulus, length, density, and so on.

\section{Part 1: Hooke's Law Forces (due before class 8 November)}

\subsection{Background}

Ultimately we will have many masses, but a good way to develop this code is to start with two fixed masses and one in the middle that can move, and then increase this to two fixed and two movable masses. Then you will extend things to $N$ masses.

Your simulation will have the following properties:

\begin{itemize}
	\item Each node has a mass $m$
	\item Each spring has a spring constant $k$
	\item Each spring has an unstretched (equilibrium) length $r_0$
\end{itemize}

Hooke's law says that if a spring runs between two points $\vec s_i$ and $\vec s_j$, then the force it applies to the point $r_i$ has a magnitude

$$F = k(r_{ij} - r_0)$$

where $r_{ij}$ is the magnitude of the separation vector given by

$$r_{ij} = ||\vec s_j - \vec s_i||$$

We also need the distance of this force. If $r_{ij}$ is greater than $r_0$ (the spring is being stretched), then the force on point $\vec s_i$ points from $\vec s_i$ to $\vec s_j$. We can describe its direction with a unit vector:

$$\hat F = \hat r_{ij} = \frac{\vec s_j - \vec s_i}{||\vec s_j - \vec s_i||} = \frac{\vec s_j - \vec s_i}{r_{ij}}.$$

We can put these pieces together to write the force vector as a proper vector:

$$\vec F = F \cdot \hat F = k(r_{ij} - r_0) \hat r_{ij}$$

Note that this is the force from {\it one} spring; each mass will be linked to the masses on either side by {\it two} such springs, so the total force on it will be the sum of two such terms.

\subsection{Assignment}

\begin{enumerate}
	\item Write code to simulate a system with three nodes: two masses that are fixed in place (perhaps at locations (0,0) and (2,0)) and one movable mass that's free to move connected by springs to the two fixed masses. Your code should allow you to easily change $k$ and $r_0$ and to put the movable mass anywhere you like as an initial condition.
	
	\item Play with your code and verify that it works as expected. Submit a screenshot of the animation with your report. (Note that shift-P takes a screenshot in {\tt anim} -- this won't work on Macs, though.)
	
	\item Now, extend your code to use arrays for the positions and velocities of the three masses. In Python, this will entail using a {\it two-dimensional} array; in C, this will be an array of vectors. We will talk about how to do this Friday.
		
	\item As an interim step, if you wish, extend your code to {\it two} movable masses (four total). This means, for instance:
	
	\begin{itemize}
		\item Mass 0 is fixed and doesn't move
		\item Mass 1 feels forces from mass 0 on its left and mass 2 on its right
		\item Mass 2 feels forces from mass 1 on its left and mass 3 on its right
		\item Mass 3 is fixed and doesn't move
	\end{itemize}
	
	Play with this a bit by changing the initial positions of the two movable masses and confirm that it moves ``like you expect for it to''.
	
	You don't have to do this step if you are confident in your ability to use arrays, and you may move on to $N$ such masses instead (the next step).
	
	\item Extend this system to $N+1$ total masses separated by $N$ links, where $N$ might be dozens or hundreds.  Later you will want to change the following things, so use variables that you can tweak for:
	
	\begin{itemize}
		\item The number of links $N$, which will give $N+1$ masses, the inner $N-1$ of which are able to move
		\item The mass $m$ of each of the masses
		\item The spring constant $k$ of each spring linking them
		\item The equilibrium length $r_0$ of each of these springs
		\item The total length $L$ of your string (which should be longer than $N r_0$ -- since we are simulating a guitar string we want to stretch it)
	\end{itemize}
	
\end{enumerate}


\end{document}
