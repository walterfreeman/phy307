---
layout: page
title: Project 4
permalink: projects/project4.html
use_math: true
---
<center>

This assignment is posted as a <a href="hw-pendulum.pdf">PDF</a> so the mathematics looks better. It is due before class Tuesday, October 15; you should have your data ready by the end of class Thursday, October 10, and we will have a checkpoint for this in class.

</center>
