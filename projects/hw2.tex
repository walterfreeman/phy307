\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\begin{document}
\Large
\centerline{\sc{Physics 307 Project 2}}
\centerline{Due before class on Monday, 25 September}
\normalsize

In this project, you will extend and modify your integration routine to study the numerical error in integration.

Remember: this class intends for you to ask us lots of questions as you work. Computational physics is best learned in 
small conversations in the context of you trying to solve problems, not from a lecture. Ask us stuff early and often:

\begin{itemize}
\item in person in class
\item over Slack
\item over email
\end{itemize}

\begin{enumerate}

\item Ask us at least one question in the \#physics307 channel on Slack while you're working on your code. Alternatively, post your favorite meme to the \#physics307 channel on Slack. (We want to make sure everyone has Slack working so you can ask us questions!)

\item{Write a separate function that performs your integration. It should take as arguments:
	
	\begin{itemize}
		\item The lower bound of the integration interval
		\item The upper bound of the integration interval
		\item The number of integration bins to use. (You could pass in the stepsize instead, but
		in that case make sure you only use stepsizes that go into the interval evenly!)
	\end{itemize}
	
	This will make your life much easier in the long run.}

\item{Analytically compute the value of $$\int_{0}^{2} \sin x\, dx.$$ Then use your integration routine to compute it numerically, using a range of Riemann sum stepsizes from $10^{0}$
    to $10^{-7}$. It will be easiest if you automate this last process by using a loop to cycle through the various values of your stepsize; this is where putting your integrator in a separate function will save you work! Do this using the left-hand rule, if you have been doing something else. 
  Then make a log-log plot of the error (magnitude of the difference between the analytic and numeric answers) vs. the stepsize. You can make log-log plots using {\tt plot -l}. 

}

\item{Now, write another integration routine that uses the midpoint rule. (Copy-paste from your previous function may save you a lot of work...) Again make a log-log plot of the error as a function of the stepsize, using stepsizes from $10^{0}$ to $10^{-7}$.
    Plot these graphs for both the midpoint rule and the left-hand rule on the same axis. Do the graphs look like you expect? How do you explain their behavior for large stepsizes?
  For very small stepsizes? How do you interpret their slopes?}

\item{Repeat the previous problem using the trapezoid rule. Does it behave as you expect it to?}

\item{If you are working in C, modify one of your second-order integrators to use {\tt double} variables instead of {\tt float} or vice versa. Does this have the effect you expect on the error?}

\item{Finally, use one of your second-order integrators to compute $$\int_0^2 \sqrt{x}\,dx.$$ Plot the error vs. the stepsize again. Why do you think this is behaving badly?
  Hint: Compare this to a similar plot for $$\int_1^2 \sqrt{x}\,dx.$$}

\item{{\bf Extra credit:} Write an integrator that uses Simpson's rule. As we will discuss in class, the easiest way to do the Simpson's rule computation is to take 1/3 of the trapezoid rule result and add it to 2/3 of the midpoint rule result. Make the log-log error plot as before. Does it perform as you expect?}
  
  \item {{\bf Lots of extra credit:} By using a linear combination of the trapezoid rule and the midpoint rule, you can cancel out the leading order error terms
  	in the trapezoid rule and the midpoint rule to form Simpson's rule. Since this cancels out the second-order error, this makes Simpson's rule a third-order method... or
  	does it? Actually, by a miracle of symmetry, the {\it third-order} errors cancel as well, so Simpson's rule is actually {\it fourth-order}. 
  	
  	
  	By a similar process to that in
  	the notes on numerical integration, but carrying the Taylor series to one more order, prove that the third-order error term cancels as well.}
  \end{enumerate} 
\newpage

FAQ:

\begin{itemize}

\item {\it Some of my data don't show! I get errors when I try to plot!}

\begin{itemize}
\item Look at your data file and make sure your values are sensible. Remember that if you're making a log-log plot, you can only plot positive values -- you may need the {\tt fabs()} function (C) or the {\tt abs()} function (Python) to take absolute values.
\end{itemize}

\item {\it I get funny data! My error goes down for smaller stepsizes like I expect, but then back up again, or my error goes down for a little while and then stops!} 

\begin{itemize}
\item Did you finish question 6, where you use {\tt double} variables? What happens then? Does it behave like you expect now? Does this give you any insight into what is going on?

\item Are you typing in a hard-coded value for the exact value of the integral? If so, is it of comparable precision (as many digits) as the numerical calculation you are doing? In general, you'll have better luck using an analytical expression for the exact answer (and letting the computer figure out what it is).
\end{itemize}
\item {\it What stepsizes should I use?} 

\begin{itemize}
\item You want a range that's evenly scattered -- in {\it logarithmic space} -- in the range. So, instead of {\it adding} a fixed value to your stepsize in your outer {\tt for} loop, maybe you could {\it multiply} (or divide) by a fixed value? In Python, either write a {\tt while} loop to do the integration, or investigate the {\tt logspace} function that is part of {\tt numpy}.
\end{itemize}
\item {\it My error bounces around strangely!} 

\begin{itemize}
	\item Are you using stepsizes that go into the integration interval evenly?
	
\item For people programming in C: Remember the ``last step rounding bug'' that we might talk about in class. In brief, you want {\tt for(x=a; x<b-dx/2; x=x+dx)} to avoid the rounding issue that happens at the last step. It is even better to do {\tt for (bin=0; bin<nbins; bin=bin+1)} and then calculate what {values of {\tt x}} on the fly, to avoid rounding issues entirely.
\end{itemize}

\item {\it My integrator is stuck -- it is suddenly taking forever to run and not stopping!}
\begin{itemize}
	\item If you are using Python, it may take a little while for very small stepsizes. Remember that Python is about a hundred times slower than C; this is usually fine but sometimes means you'll need to wait a minute or two.
	\item Ask Walter or Gwen about this if you are using C. There's a subtle but interesting rounding bug that can happen.
\end{itemize}
\end{itemize}
\end{document}
