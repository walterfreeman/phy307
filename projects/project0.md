---
layout: page
title: Project 0
permalink: projects/project0.html
usemathjax: true
---
<center>

<b>Due Monday, 2 September, by the end of the day</b><br>

</center>

<br>

---
Remember, to submit your assignment, email to <suphysics307@gmail.com>. Include all your code (`.c` or `.py` files), your graph (the PDF of your plot), and your report.
This report will be short (since in the beginning we're mostly interested in developing some computer skills, rather than doing physics), but the reports will
be a larger and larger portion of your submission as the semester goes on.

---

<br>

0. Join the [Slack team for Syracuse University Physics](https://suphysics.slack.com), the [Discord server](https://discord.gg/SYuYbz44bp), or both. 
(There is a #physics307 channel on both.) Say hi -- introduce yourself, be silly, and ask questions! This is one of the best places for you to get help with your code, from your classmates or from me. We are sharing the Slack team with the whole department, so make sure you join the #physics307 channel and talk there, rather than in #general.

1. Use a text editor to create a text file with the following information:
    - Your name, NetID, major, and academic year
    - Do you have any programming experience? If so, in which languages? 
    - Tell us a bit about your computing environment:
      - Are you using a Mac, a Windows machine running Windows Subsystem for Linux, a Linux machine, or something else?
      - Did you have any trouble getting things set up?
      - What text editor did you use to make this file?
      - Do you plan to use C, Python, or some other language you know for this class? (If you're still deciding, that's okay.)
    - What physics classes have you taken/are you taking now?
    - What math classes have you taken/are you taking? (You don't need to list calculus classes if you are beyond the calculus sequence.)
    - Are there any physical systems you are interested in modeling in this course? What would you like to simulate or study?
Include this file with your submission.

2. Write a program that reads in a temperature in Celsius and prints the equivalent in Fahrenheit.
Test it to make sure the results are what they should be, and discuss any issues that arose in writing your code. 
(Even if there weren't any, include a note saying so. All of your programs should be submitted with some accompanying discussion.)


3. Write a program that prints two columns of numbers, corresponding to x and $$\sin x$$ over some
interesting range. Save the output to a file. Then modify your program to generate values for another
function of your choice over that range. Finally, use the `plot` tool to generate a PDF of a graph 
containing both functions, and submit your graph.

4. Think of a way to test (roughly) how many arithmetic operations your computer can perform per second. 
Note that printing things to the screen is a far bigger
task for the computer than adding or multiplying, so don't slow the computer down by requesting lots
of output. There are hard ways to do this -- and one very simple one. Choose the simple one! (Hint: Ask it 
to do a lot of things and see how long it takes. You can time this with your watch or the `time` command-line tool.) 
<br><br>
Talk to your classmates who have used different programming languages. (If you used C, talk to someone
who used Python, and vice versa.) How does
the speed of Python compare to the speed of C? <br><br> You can measure how long a program takes to run by doing `time <command>` at the Linux command line. 
<br><br> Discuss how your code measures this and how accurate you think your measurement is.
You should keep this value in your head throughout the semester as an order-of-magnitude estimate
for how long calculations will take.<br><br>  From now on, you may use *any language you like* to do all the assignments for this class. Whenever it is time
  to learn a new programming idea, we will make sure to teach you whatever you need to know in both C and Python.

5. Organize the files you're using for this course. You may do this in any way you want, but you probably shouldn't leave them all in the same directory.
One way to do this might be to create a directory called `phy307` in your home directory using `mkdir` for the class as a whole, then change to that directory by doing `cd phy307` and create a subdirectory called `project0` inside it.
Then you can move all the files you have created for this project into the new directory using `mv`. I'll demonstrate how to do this in class.
You can see your files in a graphical file manager too, but you should know how to do this on the command line.
