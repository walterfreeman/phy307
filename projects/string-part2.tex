\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{amsmath}
\usepackage{fullpage}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\begin{document}
\Large
\centerline{\sc{Physics 307 Vibrating String Project Part 2}}
\centerline{Report due before class Thursday, 14 November}
\normalsize

In this continuation of your simulation of the vibrating string, you have two main objectives:

\begin{itemize}
	\item Make contact between the simulation parameters that go into your model and the physical properties of a string
	\item Verify both qualitatively and quantitatively that your vibrating string model behaves like you expect a real stretched string to behave
\end{itemize}

For this part of the project, do the following:

\begin{enumerate}
		\item If you haven't done this already, modify your code to simulate $N+1$ masses with $N$ links between them. Note that you'll need to be careful about the bounds on the {\tt for} loops you use to iterate over them:
			\begin{itemize}
				\item If you have $N$ links and $N+1$ masses, the indices for those masses will be (0 ... N)
				\item Since the masses on the ends don't move, the moving masses will be (1 ... N-1)
				\item Since {\tt range()} in Python chops off the last entry in the range, doing {\tt range(1, N)} will produce a set from 1 to $N-1$
			\end{itemize}

		\item ``Stretch'' your string by generating initial conditions for your masses that are equally spaced horizontally, but whose spacing is somewhat greater than the equilibrium length $r_0$.

		\item Then play around with various initial conditions. Suppose that the stretched length is $L'$. You should try:
			\begin{enumerate}
				\item A very simple initial condition where you displace a single mass vertically (this will produce some ``interesting'' behavior)
				\item A sinusoidal initial condition

					$$y(x) = A \sin \frac{n \pi x}{L'}$$

					where $A$ is the amplitude and $n$ is a small integer. Describe in your report how this behaves for different values of $n$ and for values of $A$ that are both much smaller than $L'$ and comparable to $L'$.

				\item A Gaussian initial condition 

					$$y(x) = A e^{\frac{-(x-x_c)^2}{\sigma^2}}.$$

					A Gaussian is a ``bell curve'' or ``bump''; in this case, 
					you're creating a bump in the middle of your string with 
					amplitude $A$, centered at $x_c$, with width $\sigma$.

					Describe how this behaves in your report for different values of $\sigma$.

			\end{enumerate}
		
		\item Now you have a simulation of a vibrating string with the following ``knobs to turn'' -- parameters that you can adjust:
		
		\begin{itemize}
			\item A number of masses $N+1$
			\item The mass of each of them, $m$
			\item The equilibrium length of each segment, $r_0$
			\item The spring constant of each segment, $k$
			\item It is stretched between two fixed ends of the string a distance $L$ apart, or alternatively it is set up so that each link is stretched to a distance $r_s$
		\end{itemize}
	
	You can tweak these parameters and your model will behave differently. Spend some time twiddling them and seeing what happens, and how your Gaussians and sine-waves respond differently when you do.
	
	However, many of these are artificial -- they are things that exist only in your computer model, not in a real string (which is of course not divided into segments at all). A real string on a guitar, in contrast, has the following properties:
	
	\begin{itemize}
		\item An unstretched length $L_0$
		\item A volumetric mass density $\rho$
		\item A Young's modulus $E$
		\item A radius $r$ (assuming a circular cross section)
		\item A stretched length $L$, or alternatively it is stretched with a tension $T$
	\end{itemize}

    It is simpler to absorb the volumetric density, the Young's modulus, and the radius into the following:
    
    \begin{itemize}
    	\item A ``stiffness'' $\alpha \equiv EA = \pi E r^2$
    	\item A linear mass density $\mu_0 = \rho A = \pi \rho r^2$
    \end{itemize}

    Reconfigure your code so that its parameters are $L_0$, $\mu_0$, $L$ or $T$, $\alpha$, and $N$. Then run simulations with the same initial conditions but different values of $N$ and verify that you get the same behavior.
    
    \item Modify your code to measure and print out the period of oscillation of a specified normal mode. (You can most easily do this by watching the $y$ coordinate or the value of $v_y$ at an antinode and looking for sign changes, as you did for the pendulum.)
    
    The analytical solution that we expect to be valid in the small-angle approximation predicts that the period of the $n$'th normal mode will be:
    
  $$\tau_n=\frac{\lambda_n}{v}=\frac{2L}{n} \sqrt{  \frac{\mu}{T}}$$
	
	Notice here that the value of $\mu$ is the mass density {\it after the string has been stretched}, which will be somewhat less than $\mu_0$ -- stretching the string makes it thinner.
	
	Verify that your simulation reproduces the expected period in the limit where the amplitude is small.
	
\end{enumerate}


\end{document}
