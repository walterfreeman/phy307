---
layout: page
title: Project 5
permalink: projects/project5.html
use_math: true
---
<center>

This assignment is posted as a <a href="hw-gravity.pdf">PDF</a> so the mathematics looks better. It is due before class Thursday, October 24.

</center>
