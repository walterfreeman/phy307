---
layout: page
title: Project 3
permalink: projects/hw3.pdf
use_math: true
---
<center>

This assignment is posted as a <a href="hw3.pdf">PDF</a> so the mathematics looks better.

**Expected timeline:**
* Code complete: end of class on Tuesday, September 24
* Data generated: before class on Thursday, September 26 *(We will start the next topic Thursday in class)*
* Report submitted: before class on Tuesday, October 2

</center>
