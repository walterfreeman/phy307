---
layout: page
title: Project 7 (all parts)
permalink: projects/project7-1.html
category: top
use_math: true
---
<center>

The third and final part of Project 7 is posted as a <a href="string-part3.pdf">PDF</a> so the mathematics looks better. 
You should have your code completely written and data gathered by the end of class November 21; your report will be due December 15
although I encourage you to finish it sooner.

<br><br>

The second part of Project 7 is posted as a <a href="string-part2.pdf">PDF</a> so the mathematics looks better. It is due before class Thursday, November 14.

<br><br>

The first part of Project 7 is posted as a <a href="string-part1.pdf">PDF</a> so the mathematics looks better. It is due before class Thursday, November 7.
</center>
