---
layout: page
title: Project 8
permalink: projects/project8.html
category: top
use_math: true
---

### So what next?

Many of you have finished the vibrating string project and are writing your reports, but there is still time left in the semester! 

I waffled on what to do for this week, and then realized that most of you are *already* having fun exploring the things you want to explore.
So I want to present to you a few options of "what more you could learn in the next week", and then turn you loose to do whatever you 
want.

If you haven't finished the vibrating string project, you should focus your time that. It will be better for you to complete that project fully 
(including putting time and thought into writing your report) than to rush through it to do something extra.

For those who have finished, here are a few options:

### Monte Carlo calculations

A huge use of numerical methods which we haven't explored yet is the use of *randomness* to estimate quantities that you cannot calculate exactly.
For instance, you can use a random number generator to generate an estimate for π, and you can repeat that estimate as many times as you wish to
improve its precision.

If you want to work on this, I've written up a <a href="project-monte-carlo.pdf">short project on Monte Carlo estimation</a>

### Sound from the vibrating string

We've simulated guitar strings and watched them vibrate, but what about hearing them?

An electric guitar uses electromagnetic devices called *pickups* that convert the oscillations of a particular place on the string into an electrical signal,
then send that on to an amplifier. You can do that, too!

To get started on this, see the notes on <a href="https://walterfreeman.gitlab.io/phy307/notes/audio.html">sound output</a>.

This is a short program that will let you convert a data file into an audio file in `.wav` format that you can play on your speakers.

Then you can try stuff like:

* Switch back to using Gaussian initial conditions, similar to what you would get from plucking a real guitar string with your finger or a pick
* Add a physically realistic drag force to your string, so that each mass feels a drag force proportional to its velocity (or velocity squared) and pointing opposite its motion; this will give a realistic "decay" to your string
* Modify your code to print out the y-coordinate of a chosen point on the string vs. time each timestep (possibly using the anim `!` passthrough character) and 
then see what it sounds like!
* You will need to fiddle around with the "speedup" parameter in `makeaudio` so that the frequencies correspond to those within the human hearing range that your computer can produce

### 3D membrane

We've done a one-dimensional thing vibrating in a second dimension; what about a two-dimensional membrane vibrating in a third dimension?

This is conceptually no more complex than what you've done already. Now you will need to put a bunch of masses in a grid and create Hooke's law forces
between each one and its four neighbors (up/down/left/right).

Anim has some fancy graphics options to make this look better; talk to me if this is what you want to do.

### Molecular dynamics

I mentioned another project involving simulating an ideal gas (or a *nonideal* gas) in a box. This entails simulating a whole bunch of particles that all exert forces
on one another, then measuring what they do.

After thinking about it a bit, I decided that I want to go through this project with you all on the last day of class together as a "semester ending" thing. I'll send
you the code I am using ahead of time so you can play with it along with me, though!

