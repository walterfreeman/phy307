from numpy import *

def divisible(n, i):
    if (n%i==0):
        return 1
    else:
        return 0

def prime(n):
    for i in range(2, int(sqrt(n)+1)):
        if divisible(n, i):
            return 0
    return 1

def countprimes(n):
    nfound = 0
    for i in range(2, n+1):
        nfound = nfound + prime(i)
    print ("%d primes below %d" % (nfound, n))


countprimes(500000)
