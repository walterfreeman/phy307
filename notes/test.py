from numpy import *
#left-hand rule integral calculator
def leftintegrate(lower,upper,step):
    i = lower
    total = 0
    while i < upper:
        value = sin(i) * step
        total += value
        i += step
    return total
#left-hand rule stepsizes
def leftlist():
    z = 0
    while z >= -7:
        print("Stepsize: ",10**z," Value: ",leftintegrate(0,2,10**z)," Error: ",abs(1.416146836547142-leftintegrate(0,2,10**z)))
        z -= 1
#midpoint rule integral calculator
def midintegrate(lower,upper,step):
    i = lower
    total = 0
    while i < upper:
        value = sin(i+(step/2)) * step
        total += value
        i += step
    return total
#midpoint rule stepsizes
def midlist():
    z = 0
    while z >= -7:
        print(10**z,abs(1.416146836547142-midintegrate(0,2,10**z)))
        z -= 1
#trapezoid rule integral calculator
def trapintegrate(lower,upper,step):
    i = lower
    total = 0
    while i < upper:
        value = ((sin(i)+sin(i+step))/2) * step
        total += value
        i += step
    return total
#trapezoid rule stepsizes
def traplist():
    z = 0
    while z >= -7:
        print("Stepsize: ",10**z," Value: ",trapintegrate(0,2,10**z)," Error: ",abs(1.416146836547142-trapintegrate(0,2,10**z)))
        z -= 1
#simpsons rule integrate
def simpsons(lower,upper,step):
    x = lower
    y = upper
    z = step
    return (trapintegrate(x,y,z))/3 + (midintegrate(x,y,z))*(2/3)
#simpsons rule list
def simpsonslist():
    z = 0
    while z >= -7:
        print("Stepsize: ",10**z," Value: ",simpsons(0,2,10**z)," Error: ",abs(1.416146836547142-simpsons(0,2,10**z)))
        z -= 1



midlist()
