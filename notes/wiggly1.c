#include <math.h>
#include <stdio.h>

double f(double x)
{
	return sin(x);
}

double exact(void)
{
	return cos(0)-cos(2);
}

double trap(double lb, double ub, double h)
{
	double sum=0;
	for (double x=lb; x<ub-h/2; x+=h)
	{
		sum+=(f(x) + f(x+h))/2*h;
	}
	return sum;
}

double mid(double lb, double ub, double h)
{
	double sum=0;
	for (double x=lb; x<ub-h/2; x+=h)
	{
		sum+=(f(x+h/2))*h;
	}
	return sum;
}

double lh(double lb, double ub, double h)
{
	double sum=0;
	for (double x=lb; x<ub; x+=h)
	{
		sum+=(f(x))*h;
	}
	return sum;
}


int main(void)
{
	for (int points=1; points < 1e9; points = points * 10)
	{
		double h=2.0/points;
//		printf("%e %e\n",h, fabs(mid(0, 2, h)*2./3. + trap(0, 2, h)*1./3. -exact()));
                printf("%.15f %.15f %10d %6.2f\n",h, fabs(2./3. * mid(0, 2, h) + 1./3. * trap(0, 2, h) - exact()), points, sqrt(points));
	}
}
