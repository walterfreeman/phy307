---
layout: default
category: Notes
navtitle: Symplectic solvers
permalink: notes/symplectic.html
use_math: true
usemathjax: true
---

These notes are typeset as a <a href="symplectic.pdf">PDF</a> so the mathematics looks better.
