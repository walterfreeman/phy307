# In-class mini-project: Loops in Python and Graphing Functions

## Prerequisites:

* You've got Python working on your laptop (if not, ask Walter or Gwen for help)

## Loops

To make a graph of a function (or to do any other interesting mathematics on a computer), we need to know how to make the computer
*repeat something*. We might want to put 100, or 1000, or 10000 points on our graph, after all.

Consider the following piece of code in Python:

```
x=0
while (x<10):
    print (x,"squared is",x*x)
    print (x,"cubed is",x*x*x)
    x = x+1
print ("All done!")
```

Discuss with your partner what you think `x = x+1` does. Remember that the `=` operator in Python is an "assignment" operator: it calculates the
value on the right and stores it in the variable on the left.

Try to guess what this will do -- and guess what the *last* line it will print is. Then run it on your computer to see if you're right.

Notice that only the *indented* portion of code is repeated. Python uses indentation like this to indicate blocks of code.

Here's another piece of Python code that demonstrates another way to make loops:



```
for x in range(10):
    print (x,"squared is",x*x)
    print (x,"cubed is",x*x*x)
print ("All done!")
```

Discuss with your partner what you think the `range()` function does here. Then change your code to the following and see what happens:

```
for x in range(10, 30, 4):
    print (x,"squared is",x*x)
print ("All done!")
```

What do the three arguments to `range` do?

### NumPy and the `arange()` function

`range()` can't use decimal (floating-point) numbers. If you want to use these, you'll need the `arange()` function. 

However, this isn't part of standard Python; it's in the NumPy library. You will need to `import` this library before you use it. 

Guess what the following code does, then run it:

```
from numpy import *
for x in arange(0, 5, 0.5):
    print (x, x*x)
```

## Graphing Functions

The text editor you're using to write your code (whether `nano` or something fancier) can also create any other kind of text file.
(Remember that computer code is just text that means something specific.)

Use it to create a text file (called perhaps `sample-plot`) consisting of pairs of numbers, for instance:

```
1 1
2 1
3 2
4 3
5 5
6 8
7 13
```

Save this file and then type `plot sample-plot` at the terminal.

### Output redirection

You now know two things:

* How to make a computer program repeat a calculation many times using a loop (using either `while` or `for`)
* How to generate a graph from a text file consisting of two columns of numbers

You could write a computer program to print two columns of numbers, copy-and-paste it into a text file, and then use `plot` to graph the text file.
But in physics we are lazy and this is more work than we want to do!

So, in order to have the computer generate the text file for you, we use something called *output redirection*. This allows you to take the output
of a program and save it as a file rather than printing it to the screen.

Suppose that the previous program you wrote was called `loop.py`. Run the following command:

```python3 loop.py > loop-output```

Then run `ls` to look at your list of files. You'll notice a new file called `loop-output`. Look at that file in your favorite text editor. 
`>` is called the *output redirection operator*; it takes the output of a command and saves it to a file.

Now you know everything you need to graph functions. You can write a computer program to generate two columns of numbers, then redirect its output
to a file using the `>` operator, then use `plot` to graph that file!

## Next steps

Once you've made your graph, try graphing two functions at once! This is part of Project 0.
