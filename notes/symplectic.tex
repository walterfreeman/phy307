\documentclass[12pt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage[margin=2.5cm]{geometry}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"

\begin{document}
	\pagenumbering{gobble}
\Large
\centerline{{Notes on solving second-order differential equations}}
\centerline{(Including symplectic solvers)}
\normalsize

\section{The problem}
Often, when simulating oscillatory systems like pendulums, orbits, guitar strings, drumheads, and many others, we are primarily concerned with {\it stability}. A simulation that drifts away from the exact solution over many periods
is acceptable if it preserves the general character of what it's supposed to do.

We know that when using Euler and RK2 solvers, we accumulate an error proportional to $dt$ or $dt^2$ over a simulation time $t$. We've not discussed how these errors depend on $t$ itself. In general, this is highly dependent
on exactly what system you are solving, and can be complicated.

As with all physical observables, the energy of the system will also have an error proportional to $dt$ or $dt^2$ for these solvers. Since energy is conserved, even if we can't write down the exact behavior of our system (for instance,
with our pendulum there is no analytic answer for $\theta(t)$), we know one property of the exact solution: the energy is a constant. (In this case, the energy is $\frac{1}{2}I\ddot\theta + gL \cos \theta$.)

Here's the bad news: the violation of conservation of energy is often {\bf exponential} in time. This means that {\bf dividing the timestep by $X$} (making the computer do $X$ times as much work!) will only produce a linear increase proportional to $\log X$ in
how long the simulation will run before blowing up. This is very, very bad if we want to simulate an oscillatory or near-oscillatory system for any significant length of time.

\section{Symplectic solvers}


The Euler and RK2 solvers you already learned about are general-purpose: they can be used to solve any system of differential equations, regardless of whether they have their ultimate origin in Newton's laws of motion or not. However, there
is a special class of solvers, called {\it symplectic solvers}, which can only be used on physical systems whose dynamical variables consist of positions and velocities. Symplectic solvers have the special property that they conserve energy
far better than their nominal order would reflect, and that in particular they are stable in oscillatory systems.

\subsection{The Euler-Cromer solver}

Consider this seemingly-straightforward implementation of the Euler solver:

\begin{verbatim}
Repeat:
	x = x + v * dt;
	v = v + a(x) * dt;
\end{verbatim}
		
		This doesn't actually implement Euler! Recall that the Euler prescription is to update both position and velocity based on the state at the {\it beginning} of the step, but here we've updated velocity based on the already-updated position! (To see how Euler
		is actually done, see the end of this file.)
		
		This is a different solver, called the {\it semi-implicit Euler method}, or the {\it Euler-Cromer method}. Like Euler, it is a first-order method. However, it is a {\it symplectic} integrator, so it conserves energy much better than we'd expect from a
		first-order method (say, Euler). Why this is is not obvious and is somewhat beyond the scope of this class; it's good enough to know that it is the case.


\subsection{The ``backwards" Euler-Cromer solver}

We could also implement Euler-Cromer like this, using the old value of position to update velocity, and the new value to update position:

\begin{verbatim}
Repeat:
	v = v + a(x) * dt;
	x = x + v * dt;
\end{verbatim}
		
This is also a first-order symplectic solver.
		
\section{The leapfrog solver}
		
		In each of these approaches, either the position or the velocity is "ahead" of the other. We might imagine an alternate approach that alternates the two Euler-Cromer methods:
		
\begin{verbatim}
Repeat:
	x = x + v * dt/2;
	v = v + a(x) * dt/2;
	v = v + a(x) * dt/2;
	x = x + v * dt/2;
\end{verbatim}
				
				This method advances both position and velocity by {\tt {dt}}, without privileging one over the other. I'll state, but not prove, that this is a {\it second-order symplectic} integrator, and is the one that we'll use for the rest of this class.
				This has the advantage of being pretty simple to code (no ``half" variables) and of conserving energy better than RK2.
				
				We could of course rewrite this:
				
\begin{verbatim}
Repeat:
	x = x + v * dt/2;
	v = v + a(x) * dt;
	x = x + v * dt/2;
\end{verbatim}

						
						Since evaluating $a(x)$ is the time-intensive part of these calculations, this reduces the workload by half. One could also imagine doing the following:
						
\begin{verbatim}
x = x + v * dt/2;
Repeat:
	v = v + a(x) * dt;
	x = x + v * dt;
\end{verbatim}	

This also gives second-order symplectic behavior, but has the disadvantage that you never have access to $x$ and $v$ at the same time. This is important if we want to calculate the total energy, for instance.

\section{Summary of integrators}

\subsection{Euler: first order}

\begin{verbatim}
Repeat:
	xnew = x + v * dt;
	vnew = v + a(x) * dt;
	x = xnew;
	y = ynew;
\end{verbatim}	

\subsection{RK2: second order}
		
\begin{verbatim}
Repeat:
	xhalf = x + v * dt/2;
	vhalf = v + a(x) * dt/2;
	x = x + vhalf * dt;
	v = v + a(xhalf) * dt;
\end{verbatim}	
				
				
\subsection{Euler-Cromer, position first: first order symplectic}
				
\begin{verbatim}
Repeat:
	x = x + v * dt;
	v = v + a(x) * dt;
\end{verbatim}	
						
\subsection{Euler-Cromer, velocity first: first order symplectic}
						
\begin{verbatim}
Repeat:
	v = v + a(x) * dt;
	x = x + v * dt;
\end{verbatim}	
								
\subsection{Leapfrog: second order symplectic}
								
\begin{verbatim}
Repeat:
	x = x + v * dt/2;
	v = v + a(x) * dt;
	x = x + v * dt/2;
\end{verbatim}	
										
										{\bf Important note:} For most physical systems there is more than one pair of dynamical variables. (For instance, $x$, $y$, $v_x$, $v_y$ for the orbit.) In the above descriptions, the {\tt x} updates are a shorthand to update {\it all} position variables, and the
										{\tt v} updates a shorthand to update {\it all} velocities.


\end{document}
