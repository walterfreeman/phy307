---
layout: default
category: Notes
navtitle: Log-log plots 
permalink: notes/log-log.html
use_math: true
---

These notes are available as a PDF <a href="notes-loglog.pdf">here</a> to make the math look pretty.
