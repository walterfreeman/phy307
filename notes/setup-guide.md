---
layout: default
title: Setup Guide 
permalink: notes/laptop.html
use_math: true
---

<center><h1>How to use your own laptop for Physics 307</h1></center>

You can set up your own computer for this class in a few different ways depending on whether it runs Linux, macOS, or Windows.

If you don't have a laptop or yours breaks, we can loan you a Linux laptop to use. Please ask Walter for details.

### If you have a Mac

Open a terminal window by hitting command-spacebar and typing ```terminal```.

See if you have a C compiler already installed. Type ```gcc -v```. If it says something that looks like the following, then great!

```
Apple clang version 15.0.0 (clang-1500.3.9.4)
Target: arm64-apple-darwin23.5.0
Thread model: posix
InstalledDir: /Library/Developer/CommandLineTools/usr/bin
```

If not, you'll need to install the "command-line developer tools" from the Apple Store. It will give you an option to do this. Click on
the option to do the install, wait until it finishes, and then try ```gcc -v``` again in the terminal.

Then we will need to install some other software. We will use a package manager called Homebrew to use this, which brings the 
simplicity of Linux software management to macOS. 

Go to <http://brew.sh> and install Homebrew by following the instructions. It will give you a command to paste into your terminal; 
do that. It will prompt you for your password -- this is your password on *your computer*, not your NetID password.

Once you run that command, it will give you two more commands after "Next steps". Run those two commands.

Now, we'll use Homebrew to install some of the software we will need. 

First, you'll want a python interpreter. Do ```brew install python3```. It will automatically install Python for you.

Then you'll want to install the software we will need for visualization. 


Then in the terminal just type ```brew install gnuplot```. This may

Now, install the Physics 307 specific software by doing:

```
git clone https://github.com/walterfreeman/suphysics307.git
cd suphysics307
make anim-mac
make membrane-demo
sudo make install
```

Test the plotting software:

``` 
plot test-plot  
```

You should see a graph. (You'll make lots of graphs like this this semester.) It may take a little extra time to run for the first time.

If it doesn't work, try installing a package called "xquartz" by doing ```brew install xquartz --cask```. (The installer may ask for your password.) 
This install may take a little while; after it finishes, try running ```plot test-plot``` again.

First, you'll want to install xquartz, which will give you nice graphical output from gnuplot. To do this, type ```brew cask install xquartz```. 

### If you have a Windows machine

Thanks to Jada Garofolo for helping me test the installation and writing a very useful protocol! Here are the steps they followed:

1. Open PowerShell

2. Run the following command:

	```wsl --install```

3. Accept Windows Linux Interface and Windows Host to make changes to your system (to actually install the programs)

4. Once fully installed, reboot the system

5. Open WSL via the shortcut provided in your system - should be a little penguin icon :)

6. Run the following commands:
	
    ```
	sudo apt update
	sudo apt upgrade
    sudo apt install freeglut3-dev libglew-dev libpng-dev imagemagick perl libpng-dev build-essential gnuplot
    ```

7. Select "Y" when prompted during installation

8. Run the following command:

```
git clone https://github.com/walterfreeman/suphysics307.git
cd suphysics307 
make
sudo make install
```

9. Test the plotting program:

``` 
plot test-plot  
```

You should see a graph. (You'll make lots of graphs like this this semester.)
 


10. Test the animation tool:

```./membrane-demo | anim```

If you see the animation, you're all set!


### If you have a Linux laptop

You're mostly good to go. I'll assume you are on a Debian-based distribution (Ubuntu, Mint, Arch) here. If you are on Fedora, ask Walter for alternate package names.

Install some needed packages:

```sudo apt install freeglut3-dev libglew-dev libpng-dev imagemagick perl libpng-dev build-essentials gnuplot```

Clone a git repository that has some specific software we'll be using and install it:

```
git clone https://github.com/walterfreeman/suphysics307.git
cd suphysics307
make
sudo make install
```

Test the plotting program:

```
plot test-plot 
```

You should see a graph. (You'll make lots of graphs like this this semester.)

Test the animation software:

```
./membrane-demo | anim
```

You should see an animation here. (We'll make animations starting in late September or so.)
