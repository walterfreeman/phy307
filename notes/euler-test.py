x = 1
dt = 0.01
k = 1
m = 1
v = 0
t = 0
while True:
    # Euler step 
    v = v + (-k/m * x) * dt    # Update velocity based on acceleration from the OLD position
    x = x + v * dt             # Update position based on the NEW velocity 
    t = t + dt

    print ("!",t, x)               # print time vs. position data to plot
    print ("c ",x,0,0.1)
    print ("l -1 1 -1 -1")
    print ("l 1 1 1 -1")
    print ("F")


