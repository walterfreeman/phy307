from numpy import *

nmax = 1000000
nprimes = 0

for n in range(2, nmax+1):                           # check numbers n from 2 to nmax to see if they are prime
    found_factor = False
    for i in range(2, int(sqrt(n))+1):               # check divisors i from 2 to sqrt(n) to see if they go into n
        if (n%i == 0):
            found_factor = True
            break                                    # stop, we found a factor, no sense wasting time

    if (found_factor == False):
        nprimes = nprimes + 1                        # if we didn't find a factor, it's prime

print (nprimes,"up to", nmax)

