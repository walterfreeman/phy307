\documentclass[12ampt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{amsmath}
\setlength{\parskip}{4mm}

% here are some definitions that I put at the top of everything that I stole from my PhD advisor.
% I have another set of them for use in Stolkin slides, etc.

\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"



\begin{document}
\Large
\centerline{\sc{Notes on log-log plots}} 
\normalsize

\section{The problem}

We are familiar with graphs that have {\it arithmetic scales}. This means that every tick mark on the axis represents {\it adding a fixed increment} to the value. 

For instance, here's a plot of the time it takes the four inner planets (Mercury, Venus, Earth, and Mars) to orbit the Sun as a function of the size of their orbits (measured as half of the long axis, ``semimajor axis''):

\begin{center}
\includegraphics[width=0.8\textwidth]{inner-planets.pdf}
\end{center}

We're very familiar with this sort of plot. Each tick mark on the horizontal axis represents 0.2 AU of orbital size; each tick mark on the vertical axis represents 0.2 years of orbital period (how long it takes to go around the Sun).

This shows these data in a clear and concise way -- we love graphs, right? Let's add more of the Solar System to it. 


\begin{center}
	\includegraphics[width=0.8\textwidth]{all-planets.pdf}
\end{center}

Now I've added the rest of the planets (and the asteroid Ceres, which lies between Mars and Jupiter) to the plot.

We now have something of a problem: I can't tell at all from looking at the graph what's going on with Mercury, Venus, Earth, and Mars! Their points are all smushed close down to the origin in the bottom left. It is impossible to tell from looking at this plot whether the four inner planets follow the same pattern as the rest of the points.

But there's more to the Solar System than just the planets! Let's throw in the Kuiper Belt objects -- including dwarf planets like Pluto and Eris.

\begin{center}
	\includegraphics[width=0.8\textwidth]{all.pdf}
\end{center}

We can't even see the planets any more -- look at the axis scales! This graph is dominated by two very distant objects -- Leleākūhonua and Sedna.

On the horizontal axis of this plot, one tick mark represents 200 AU ... but all of the planets have orbits less than around 30 AU in size! 

So this brings us to our first challenge: {\bf We need a way to make a graph that represents values that span many orders of magnitude that allows us to see trends clearly throughout the data range.} How do we show data for Leleākūhonua and Sedna on the one hand, and for Mercury and Venus on the other, on the same plot?



\section{The logarithmic axis and the geometric progression}

The solution here is to use an axis where each tick mark represents a {\it multiplication} by a fixed quantity rather than an {\it addition}. That is, we want an axis that counts 2, 4, 8, 16, 32 or $10^{-1}, 10^0, 10^1, 10^2, 10^3$ rather than 1, 2, 3, 4 or 10, 20, 30, 40.

Such a series where each interval corresponds to a multiplication is called a {\it geometric series}; the sort of series where each interval corresponds to an addition is called an {\it arithmetic series}.

So, can we make a graph where we use a geometric series to label axes? We absolutely can! Mathematicians use the term {\it logarithmic axis} to refer to this, since the distance on the paper (in centimeters) is proportional to the logarithm of the value. Here's the same data plotted with a logarithmic y-axis:

\begin{center}
	\includegraphics[width=0.75\textwidth]{semilogy.pdf}
\end{center}

Here we've successfully ``spread out'' the y-values of the points, making better use of all of the room on the plot. But something remarkable happens when we do this to both axes at once; this is called a ``log-log'' plot since both axes are logarithmic.


\begin{center}
	\includegraphics[width=0.75\textwidth]{all-log.pdf}
\end{center}

Not only can we see our data better -- they all lie on a perfectly straight line!

{\bf Clearly the fact that this is a straight line must tell us something about the pattern in our data.} But ... what might that be?

\section{Power-law relationships and log-log plots}

Often data are well described by a {\it power-law relationship} of the form

$$y = Ax^p,$$

i.e. one where the y-value is proportional to the x-value raised to some power.

Since the horizontal and vertical position on a log-log plot are proportional to the logarithm of the coordinates, let's try to rewrite this equation as a relationship between $\log x$ and $\log y$. First we take the log of both sides:

$$\log y = \log \left(Ax^p\right)$$

Then we can use the principle that $\log XY = \log X + \log Y$ to address the right-hand side. A simple way to remember this is that {\bf ``logarithms turn multiplication into addition''}.

$$\log y = \log A + \log x^p$$

But we can rewrite $\log x^p$ as $p \log x$. (A simple way to understand this property is that exponentiation is just repeated multiplication and multiplication is just repeated addition... and logarithms turn repeated multiplication into repeated addition.)

So this gives us

$$\log y = \log A + p \log x$$

which looks a lot like the equation of a line where $p$ is the slope.

{\bf This means that power-law relationships show up as straight lines on log-log plots, and the slope of that line tells us the power.}

\section{Determining the power}

What does ``slope of the line'' mean here? The key is that slope is still equal to rise over run, but in this case ``rise'' and ``run'' are measured in {\it orders of magnitude} (geometric change), not by the number that is added (arithmetic change).

You might be able to eyeball the slope from the graph above, possibly using a piece of paper as a straightedge as an aid. However, it's easier if we add some things on top of it.


\begin{center}
	\includegraphics[width=0.75\textwidth]{with-slope-1.pdf}
\end{center}

Here the green line is the function $y=x^1$ -- a slope of 1.

It's clear here that our function's slope is greater than 1. Let's add another one:

\begin{center}
	\includegraphics[width=0.75\textwidth]{with-slope-2.pdf}
\end{center}

Here the blue line is the function $y=x^2$ -- a slope of 2. This increases more rapidly than our function, so the power must be in between.

So in our case, we know that the function describing orbital period is

$$y=x^p$$

where $p$ is between 1 and 2. (Turns out it's 1.5.)

\end{document}



