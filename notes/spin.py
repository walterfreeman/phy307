from numpy import *
theta = 0

while True:
    x = sin(theta)        # compute coordinates
    y = cos(theta)
    theta = theta + 0.03  # change the angle

    print("!l",0,0,y,x)    # draw a line
    print("!c",y,x,0.2)    # with a ball on the end
    print("!F")
    print("l",0,0,x,y)    # draw a line
    print("c",x,y,0.2)    # with a ball on the end
    print("F")

