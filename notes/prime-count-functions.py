from numpy import *

def test_divisible(value, divisor):
    if (value % divisor == 0):
        return True
    else:
        return False

def test_prime(n):
    max_factor = int(sqrt(n))   # we only need to test up to sqrt(n)

    for i in range(2, max_factor+1):
        if (test_divisible(n, i) == True):
            return False
    
    return True                 # if we get here without finding a factor, it's prime


def count_primes(n):
    nfound = 0
    for i in range(2, n+1):
        if (test_prime(i) == True):
            nfound = nfound + 1

    return nfound


print (count_primes(1000000))
