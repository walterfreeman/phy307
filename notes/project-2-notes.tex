\documentclass[12ampt]{article}
\setlength\parindent{0pt}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx}
\setlength{\parskip}{4mm}
\def\LL{\left\langle}   % left angle bracket
\def\RR{\right\rangle}  % right angle bracket
\def\LP{\left(}         % left parenthesis
\def\RP{\right)}        % right parenthesis
\def\LB{\left\{}        % left curly bracket
\def\RB{\right\}}       % right curly bracket
\def\PAR#1#2{ {{\partial #1}\over{\partial #2}} }
\def\PARTWO#1#2{ {{\partial^2 #1}\over{\partial #2}^2} }
\def\PARTWOMIX#1#2#3{ {{\partial^2 #1}\over{\partial #2 \partial #3}} }
\newcommand{\BC}{\begin{center}}
\newcommand{\EC}{\end{center}}
\newcommand{\BE}{\begin{displaymath}}
\newcommand{\EE}{\end{displaymath}}
\newcommand{\BNE}{\begin{equation}}
\newcommand{\ENE}{\end{equation}}
\newcommand{\BEA}{\begin{eqnarray}}
\newcommand{\EEA}{\nonumber\end{eqnarray}}
\newcommand{\EL}{\nonumber\\}
\newcommand{\la}[1]{\label{#1}}
\newcommand{\ie}{{\em i.e.\ }}
\newcommand{\eg}{{\em e.\,g.\ }}
\newcommand{\cf}{cf.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\Tr}{{\rm tr}}
\newcommand{\etal}{{\it et al.}}
\newcommand{\OL}[1]{\overline{#1}\ } % overline
\newcommand{\OLL}[1]{\overline{\overline{#1}}\ } % double overline
\newcommand{\OON}{\frac{1}{N}} % "one over N"
\newcommand{\OOX}[1]{\frac{1}{#1}} % "one over X"



\begin{document}
\Large
\begin{center}
	An Epistle from the Quarantine Ward \\
	\large
	-- or --\\
	What You're Doing In PHY307 Today
\end{center}

\normalsize

So, I'm out sick with presumed-COVID. I should be back Thursday, but in the interim, Jada and Nico will be running class today. Thanks so much for covering for me, y'all!

Our only agenda item for today is for you to work on Project 2. You should take the following steps:

\begin{enumerate}
	\item Write some code that computes integrals given an upper bound, a lower bound, and a number of bins (or, alternatively, a stepsize)
	\item Using some kind of loop (more on this later), iterate over a {\it geometric series} of stepsizes from around 1 to around $10^{-7}$ and generate data for error vs. stepsize
	\item Plot those data on a log-log plot and compare it to expectations
	\item If your data don't match your expectations, figure out if it's because your code has a bug or if there's an effect you weren't expecting
	\item If there's a bug (there usually is a bug, but not always!), fix it and go back to step (2).
\end{enumerate}

Here's a discussion of each of these points.

\section{Looping over stepsizes}

Consider these two sets of stepsizes. Which one is more useful for exploring the behavior of your integrator over many orders of magnitude of your stepsize?

\vspace{8mm}

	\begin{tabular}{c|c}
	\bf 	Arithmetic series                                                                                          & \bf Geometric series                                                                                             \\
%		\textit{\begin{tabular}[c]{@{}c@{}}Each step is an addition\\ Evenly spaced on a linear plot\end{tabular}} & %\textit{\begin{tabular}[c]{@{}c@{}}Each step is a multiplication\\ Evenly spaced on a log plot\end{tabular}} \\ \hline
		0.00001                                                                                                    & 0.00001                                                                                                      \\
		0.20001                                                                                                    & 0.0001                                                                                                       \\
		0.40001                                                                                                    & 0.001                                                                                                        \\
		0.60001                                                                                                    & 0.01                                                                                                         \\
		0.80001                                                                                                    & 0.1                                                                                                          \\
		1.00001                                                                                                    & 1.0                                                                                                         
	\end{tabular}

(I'll let you consider this on your own. If you're not sure, you might think about whether the points will be evenly spaced on your plot or whether they will be ``clumped up'' to one side or the other, or just implement both and see!)

\subsection{Coding a geometric series}

You already know how to write a loop that produces an {\it arithmetic series} in C or Python.

In Python, you would write:

\begin{verbatim}
for i in range (start, stop, step):
    <stuff to be looped over>
\end{verbatim}

or if you want noninteger values:

\begin{verbatim}
from numpy import *
for x in arange (start, stop, step):
	<stuff to be looped over>
\end{verbatim}

If you prefer to do things by hand, you could always use a {\tt while} loop:

\begin{verbatim}
x = start
while x < stop:
    <do stuff>
    x = x + step
\end{verbatim}



It should be pretty clear how to use a {\tt while} loop to produce a geometric series. (What do you do to the variable each time, if you don't add something to it?)

However, is there an equivalent to {\tt arange()} that produces a {\it geometric} series? Turns out there is. The ``{\tt a}" in {\tt arange()} is short for ``arithmetic'', so the function stands for ``arithmetic range'' You'd think the equivalent function for a geometric range would be called {\tt grange()}, but instead it's {\tt geomspace()}.

For instance, here's some code:

\begin{verbatim}
	from numpy import *
	
	for i in geomspace(1, 1024, 11):
	print (i)
\end{verbatim}

Its output is:

\begin{verbatim}
	$ python test-geomspace.py 
	1.0
	2.0
	4.0
	7.999999999999999
	16.0
	32.00000000000001
	63.999999999999986
	127.99999999999999
	256.0
	512.0000000000001
	1024.0
\end{verbatim}

Here it has generated eleven numbers that are evenly spaced between 1 and 1024; since $1=2^0$ and $1024=2^{10}$, these represent powers of two, and each step is a multiplication by two. (But note the floating-point roundoff errors!)

\section{Interpreting your data}

Remember the big conclusions from the last two classes:

\begin{enumerate}
	\item On log-log plots:
\begin{enumerate}
\item A power-law function $y=Bx^p$ looks like a straight line with slope $p$ on a log-log plot
\item This sort of function can be described in words as ``$y$ is proportional to $x$ to the power $p$''.
\end{enumerate}
	\item On integration methods:
	\begin{enumerate}
		\item The left-hand rule is a first-order method; this means that the overall error should be proportional to the first power of the stepsize
		\item The midpoint and trapezoid rules are second-order methods; this means that the overall error should be proportional to the second power of the stepsize (stepsize squared)
	\end{enumerate}
\end{enumerate}
	
	This means that {\bf in an ideal case} your data should look like straight lines. But you might see some funny business. The next section is a discussion of this.
	
	\section{Why do my data look like this?}
	
	
	Look in this section to see if your data look odd; it might be a problem that I've seen before and have a discussion of here.
	
	\subsection{Wiggly Lines}
	
	\begin{minipage}{0.49\textwidth}
	Here's some data for the lefthand rule. What's happened here?
	
	\bigskip
	
	The problem here is that I'm using stepsizes that don't go into the interval evenly. This means that I'm integrating ``past'' 2 sometimes. This generates an error that's somewhat random, but roughly proportional to the stepsize. Since the lefthand rule is already a first-order method, you now have the superposition of {\it two} first-order errors:
	
	\begin{enumerate}
		\item The error proportional to $h^2$ that we derived last class, summed over $N=\frac{b-a}{h}$ bins, giving the ``usual'' left-hand rule first order error
		\item An extra error beyond this coming from integrating a bit past the upper bound, since the stepsize doesn't go into the interval evenly
		
	\end{enumerate}
	
	So you get a wiggly line that's still basically slope 1.
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\includegraphics[width=1.1\textwidth]{wiggly1.pdf}
\end{minipage}
	
		\subsection{Broken lines}
		
		Here's an {\it extremely} common result. What has gone wrong here?

		It's hard to see here, but all these points lie on one of {\it two} straight lines. The upper points lie along a straight line with slope 1, and the bottom points lie along a straight line with slope 2. 
		
			\begin{minipage}{0.49\textwidth}
				This is easier to see if I revise this plot to include many more points. (All I did was to revise the previous program to use a different geometric series for generating stepsizes.)
			
			\bigskip
			
			
			This means that the algorithm is {\it sometimes} performing as second-order as it should, and {\it sometimes} only performing as first-order.
			
			\bigskip
			
			And {\it this} is because there's an additional first-order error, caused by sometimes doing an extra integration bin but not always. What could cause this?
		\end{minipage}
		\begin{minipage}{0.49\textwidth}
			\includegraphics[width=1.1\textwidth]{broken2.pdf}
		\end{minipage}
		
	The issue here is one you've seen earlier in this notes document! The problem is that computers always round off their arithmetic in the last digit. Consider this simple {\tt for} loop:
	
	\begin{verbatim}
		for x in arange(0, 2, 0.2):
		    print (x)
	\end{verbatim}
	
	which outputs
	
	\begin{verbatim}
		0.0
		0.2
		0.4
		0.6000000000000001
		0.8
		1.0
		1.2000000000000002
		1.4000000000000001
		1.6
		1.8	
	\end{verbatim}
	
	Note that in this case some of the digits have been rounded up. This is the same way we do arithmetic with digits. 
	
	Imagine that you are trying to integrate from 0 to 2 using 3 bins and you use decimal (base-10) arithmetic. $h = 2/3 = 0.6666667$, so your bins will start at:
	
	\begin{verbatim}
	0
	0.6666667
	1.3333334
	2.0000001 <--- this one won't run since it's greater than 2
	\end{verbatim}
	
	This is what we expected.
	
But suppose you want to do 6 bins. $h = 2/6 = 0.3333333$ (again doing decimal arithmetic ``on your calculator''), so you get bins that start at:
	
\begin{verbatim}
0
0.3333333
0.6666666
0.9999999
1.3333332
1.6666665
1.9999998 <----- this is less than 2, so we will do this one -- a seventh bin when we were expecting 6!
	\end{verbatim}

You might expect this problem to only occur for decimals that are repeating. So you'd expect 0.2, 0.1, etc. to work -- but we saw above that these ``nice round numbers'' have rounding error on the computer also!

The problem is that computers think in binary, where only exact powers of two have nonrepeating representations!

So this means that you're going to do an extra bin roughly half the time if the binary arithmetic decides to ``round down", but you're fine if it ``rounds up".

This is why you have two populations of data points in the graph above: sometimes you get the desired number of bins and it's second order as you expect (100x improvement in error for 10x improvement in stepsize) and some of them have a far greater error than expected, since doing an extra bin amounts to a $\mathcal O(h)$ error.


The easiest way to fix it is to change the upper bound on your integral to 2 - h/2. That way it is guaranteed to cut off at the right point regardless of the rounding.
	
		\newpage
	\subsection{The check mark graph}
	If you go to very small stepsizes you'll see a graph like this:
	\begin{center}
		\includegraphics[width=0.6\textwidth]{limit.pdf}
	\end{center}
	
	Note that this is even worse for Simpson's rule:
	
		\begin{center}
		\includegraphics[width=0.6\textwidth]{twoplots.pdf}
	\end{center}
	
	The issue here is the limited precision with which our computers do arithmetic. The floating-point numbers used in Python, and the {\tt double} type in C, only have a precision of 15 decimal digits or so. This means that we can't {\it expect} the computer to do better than an error of about one part in $10^{15}$. Really the computer can't even do this well, since {\it the more things you add up, the more the roundoff error accumulates!}. 
	
	This is why going to the very smallest stepsizes actually {\it increases} the error -- the roundoff error gets to be bigger than the $\mathcal O(h^p)$ ``truncation error'' coming from the Taylor series we did last time.
	
	\subsection{Missing data}
	
	You might notice that some of your data are missing -- some of your stepsizes aren't on your log-log plot. This is likely because the error is negative. This is fine, but you can't represent negative numbers on a log-log plot. The fix is simple: just take the absolute value (Python {\tt abs()}, C {\tt fabs()}) of your errors before printing them.
\end{document}