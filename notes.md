---
layout: page
category: top
navtitle: Notes
title: Notes
permalink: notes/
usemathjax: true
---

<a href="string-notes.pdf">Notes on the vibrating string project</a>

<a href="array-notes.pdf">Notes on using arrays in C</a>

<a href="array-python.pdf">Notes on using arrays in Python</a>


<a href="vectors-c.html">Doing mathematics with vectors in C</a>

<a href="vectors-python.html">Doing mathematics with vectors in Python</a>

<a href="cplusplus.html">A few C++ tricks</a>

<a href="symplectic.pdf">Notes on symplectic solvers</a>

<a href="anim.html">Notes on making animations</a>

<a href="DE-notes.pdf">Notes on solving differential equations</a> 

<a href="log-log.html">Log-log plots</a>

<a href="function.html">Functions in C and Python</a>

<a href="integration-notes-latex-guide.pdf">Analysis of integration methods</a> (as pdf because of typeset mathematics)

<a href="laptop.html">Using your own laptop</a>

<a href="linux.html">A short introduction to Linux</a>

<a href="language-comparison.html">A comparison of C and Python</a>

<a href="c.html">An introduction to the C programming language</a>

<a href="python.html">An introduction to the Python programming language</a>

<a href="math.html">Doing math in C</a>

<a href="math-python.html">Doing math in Python</a>

<a href="plot.html">Plotting data</a>


<a href="samples.html">Sample programs</a>


<!--




#### Old notes

These notes will be used later in class; I'm putting the links back up
as a reference for past students.

<a href="connect.html">Connecting to the course computer</a>


<a href="audio.html">Notes on audio output</a>

-->



