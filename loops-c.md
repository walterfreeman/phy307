# In-class mini-project: Loops in C and Graphing Functions

## Prerequisites:

* You've got C working on your laptop (if not, ask Walter or Gwen for help)

## Loops

To make a graph of a function (or to do any other interesting mathematics on a computer), we need to know how to make the computer
*repeat something*. We might want to put 100, or 1000, or 10000 points on our graph, after all.

Consider the following piece of code in C:

```
#include <stdio.h>

int main(void)
{
    int i;
    i=0;
    while (i<10)
    {
        printf("%d squared is %d\n",i, i*i);
        i=i+1;
    }
    printf("All done!\n");
}
```

Discuss with your partner what you think `i = i+1` does. Remember that the `=` operator in C is an "assignment" operator: it calculates the
value on the right and stores it in the variable on the left.

Notice that there is no semicolon after `while (i<10)` -- this tells the computer computer to repeat a block of code, but doesn't actually
do anything itself, so it is *not* followed by a semicolon.

Try to guess what this will do -- and guess what the *last* line it will print is. Then compile and run it on your computer to see if you're right.

Notice that only the portion of code in curly braces is repeated. While I have indented it to make it more readable,
indentation like this doesn't actually change how it runs. (This is *different* than Python.)

Here's another piece of C code that demonstrates another way to make loops:



```
#include <stdio.h>

int main(void)
{
    int i;
    for (i=0; i<10; i=i+1)
    {
        printf("%d squared is %d\n",i, i*i);
    }
    printf("All done!\n");
}

```

Notice the syntax of `for`. It is a bit unique:

```
for (what to do when the loop starts; how long to keep going; what to do after every repeat)
```

### Floating-point numbers

Suppose you wanted to print out decimal values -- say, the squares of values from 0 to 1 at 0.1 intervals.

To do this, you can't use integer variables; you have to use floating-point ones. These are different in two ways:

* They're declared using `float` instead of `int`
* You use `%f` instead of `%d` to print them

Try the following code:

```
#include <stdio.h>

int main(void)
{
    float x;
    for (x=0; x<10; x=x+0.5)
    {   
        printf("%f squared is %f\n",x, x*x);
    }
}

```

Notice that I've changed the variable from `i` to `x`. It's tradition to use variables toward the middle of the alphabet like `i` for integers
and variables toward the outside of the alphabet like `x` for floating-point numbers. 

## Graphing Functions

The text editor you're using to write your code (whether `nano` or something fancier) can also create any other kind of text file.
(Remember that computer code is just text that means something specific.)

Use it to create a text file (called perhaps `sample-plot`) consisting of pairs of numbers, for instance:

```
1 1
2 1
3 2
4 3
5 5
6 8
7 13
```

Save this file and then type `plot sample-plot` at the terminal.

### Output redirection

You now know two things:

* How to make a computer program repeat a calculation many times using a loop (using either `while` or `for`)
* How to generate a graph from a text file consisting of two columns of numbers

You could write a computer program to print two columns of numbers, copy-and-paste it into a text file, and then use `plot` to graph the text file.
But in physics we are lazy and this is more work than we want to do!

So, in order to have the computer generate the text file for you, we use something called *output redirection*. This allows you to take the output
of a program and save it as a file rather than printing it to the screen.

Suppose that the previous program you wrote was called `loop`. Run the following command:

```./loop > loop-output```

Then run `ls` to look at your list of files. You'll notice a new file called `loop-output`. Look at that file in your favorite text editor. 
`>` is called the *output redirection operator*; it takes the output of a command and saves it to a file.

Now you know everything you need to graph functions. You can write a computer program to generate two columns of numbers, then redirect its output
to a file using the `>` operator, then use `plot` to graph that file!

## Next steps

Once you've made your graph, try graphing two functions at once! This is part of Project 0.
