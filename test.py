from numpy import *

frame_skip = 100
frame = 0

print ("font large")

for circles in range(20):
  for theta in arange(0, 2*pi, 0.0001):
    x=cos(theta)
    y=sin(theta)
    if (frame % frame_skip == 0):
      print("l",0.0, 0.0, x, y)  # draw a line
      print("c", x*1.05, y*1.05, 0.05) # draw a circle
      print("t",x*1.5, y*1.5)  
      print("theta = %.2f" % (theta))   # these last two lines together draw some text
      print("F") # flush frame

    frame=frame+1
  print("!Completed",circles,"cycles")

