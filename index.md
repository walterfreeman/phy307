---
layout: default
permalink: index.html
---

<h1> Welcome to Physics 307!</h1>
y
### 3 December agenda

* Touch base with everyone on Project 7
* Introduce what we're doing for the rest of the semester, posted at <a href="projects/project8.html">Project 8</a>
* New notes:
  * <a href="notes/audio.html">Notes on audio output</a>



### 7 November agenda

* Introduce Project 7b (note the link update)
* Talk about the connection between microscopic and macroscopic properties of the vibrating string model
* Talk about the expected small-amplitude behavior of the vibrating string

### 31 October agenda

* Introduce Project 7 and start on Project 7a (the first part of the vibrating string project)
* Introduce the idea of arrays in Python (used for things other than Cartesian vectors) and C, used to store large collections of numbers that are referenced by index

New notes:
* <a href="notes/array-python.pdf">Arrays in Python</a>
* <a href="notes/array-notes.pdf">Arrays in C</a>

### 24 October agenda

* Teach computers to do vector math:
  * in Python, by using NumPy "arrays" to emulate vector math -- see <a href="notes/vectors-python.html">the notes on vectors in Python</a>
  * in C, by using a vector library that creates a new data type -- see <a href="notes/vectors-c.html">the notes on vectors in C</a>
* Introduce Project 6

### 17 October agenda

* Discuss simulating motion in two dimensions
* Discuss simulating orbital motion
* Look at a few anim tricks to make this pretty
* Introduce Project 5

### 8 October agenda

* Talk about measuring the period of a pendulum
* Here's an example of a <a href="Project_2_Report.pdf">well-written Project 2 report</a> if you'd like to take a look.

### 3 October agenda

* Discuss an approach to solving second-order differential equations, discussed in the <a href="notes/DE-notes.pdf">previous not</a>
* Introduce <a href="notes/anim.html">how to make animations</a>
* Introduce <a href="https://walterfreeman.gitlab.io/phy307/projects/hw-pendulum.pdf">Project 4</a> 
* Discuss the small-angle analytical solution to the swinging pendulum

### 24 September agenda

* Introduce the <a href="notes/DE-notes.pdf">numerical solution of differential equations</a>
* Introduce <a href="projects/hw3.pdf">Project 3</a>
* Have a quiz (possibly)

### 19 September agenda:

Today's agenda will involve a bit of looking back:

* Touch base after the class I missed while sick and see how you're doing
* Talk about how to write a good report
* Talk about the schedule going forward a bit

... but mostly looking forward:

* Introduce the <a href="notes/DE-notes.pdf">numerical solution of differential equations</a>
* Introduce <a href="projects/hw3.pdf">Project 3</a>

### 17 September agenda:

**I am home sick with COVID**. This means that Jada and Nico will be running class today. However, I'll be at home (grading your projects, since I
feel well enough to do that now), and can give instant answers to anything on Discord or Slack or by email. 
I can also talk to anyone on the phone/Zoom who wants.

All I was planning on doing today is working on Project 2. This is the sort of thing where you should work on your own code and data and ask questions 
as you have them. However, I've written a detailed set of notes that outlines a framework for you to work in, and has some common "Why do my data look
like this?" scenarios. 

**Here are the <a href="notes/project-2-notes.pdf">Notes on Project 2</a>.**

I suggest you read sections 1 and 2, then hack on your code; if you see something weird in your data, look in Section 3 to see if there's a discussion
of it.

I hope I'll be back Thursday! **Note: I am delaying the due date of Project 2 until before class next Tuesday**, but I expect that you will be done 
with everything except your writeup by class Thursday. We'll discuss how to do a good project report in class Thursday; then you have all weekend to 
write it up.


### 12 September agenda:

* Recap Project 1
* Introduce <a href="projects/hw2.pdf">Project 2</a>
* Talk about <a href="notes/integration-notes.pdf">perturbative analysis of integration methods</a>

### 10 September agenda:

* Introduce Project 2 very briefly for those who are ready to start it
* Talk about [log-log plots](notes/log-log.html)
* Finish Project 1 and/or start Project 2

### 5 September agenda:

* Introduce the idea of functions (see the [notes on functions](notes/function.html))
* Talk about how to do the "prime number counter" part of Project 1
* Introduce the idea of modularity
* Do an in-class [pair programming exercise applying the idea of modularity to Project 1](in-class/modularity.html). This exercise will be worth a small amount toward your grade.

### 3 September agenda:

* discuss Project 0 and any issues that came up
* talk more about variable types, how assignment and math works, and conditionals
* get started on [Project 1](projects/project1.html)


### 29 August agenda:

* talk about the syllabus a bit, since we didn't do that before :)
* talk about how to graph functions: see the [notes](notes/graphing.html)
* introduce [Project 0](projects/project0.html)


### 27 August agenda:
* talk briefly about what this class will be, and what you can expect
* introduce you to the folks teaching this class
* **get everyone set up in a terminal environment on their own computer to work in this semester**
* introduce some basic Linux commands and the C and/or Python programming languages
* guide everyone through writing their first program!

In more detail, here are the instructions for the first day:


* [Get your computers set up](https://discord.gg/4YhBDNZx) to do computational physics:
  * Set up a terminal environment to program in
  * Install the software we'll be using for our class (a tool to make plots)
* Join the Slack channel at [suphysics.slack.com](https://suphysics.slack.com) or the [Discord server](https://discord.gg/4YhBDNZx) and send memes/say hi
* Get started writing code in C, Python, or both:
  * You can find notes about both C and Python on the [notes](notes/) page, along with a comparison of the two. I'll also demonstrate
    both.


<!--
This is some text.


### 5 November agenda:

* Talk about how to go about attacking this string project
* Talk about programming with arrays in both C and Python
* Get started hacking! :)

### 10 October agenda:

* Do a retrospective on Project 4
* Talk about systems that move in more than one dimension
* Introduce Project 5


### 1 October agenda:

* Talk about "perfect" data vs. the kind of data we usually get
* Introduce second-order differential equations
* Introduce animations and get [anim](anim.tar) set up on your computers
* Introduce Project 4

New notes:

* [Symplectic solvers for Newton's laws](notes/symplectic.html)
* [Making animations](notes/anim.html)
* [A few C++ tricks that are useful](notes/cplusplus.html) <it>(Optional, may make life easier for C programmers)</it>


### 19 September agenda:

* recap your results from Project 2
* introduce numerical solution of differential equations
* introduce Project 3
* watch (a bit of) a movie!

### 10 September agenda:

* talk about written reports and our expectations
  * See [a report from a student last year](laurel-white-project-5.pdf) as an example of what we're after
  * Please don't emulate this format exactly -- this is an example of how to discuss data in general, not a model to copy
* talk about Taylor series and what they are good for
* talk about analysis of algorithms
* talk about how to use Taylor series to analyze the error in numerical integration methods

New assignment: [Project 2](projects/hw2.pdf)

New notes: [Analysis of numerical integration methods](notes/integration-notes.pdf), [programming with functions](notes/function.html)

### 3 September agenda:

* ensure everyone has finished Project 0
* talk about a few programming concepts you'll need for Project 1
* get started working on Project 1, the first "real" assignment for our class

### 29 August agenda:
* introduce you to the C programming language
* introduce you to a few other programming ideas in both Python and C
* demonstrate the ideas behind computational physics and the power of the discipline
* turn you loose on Project 0







-->
